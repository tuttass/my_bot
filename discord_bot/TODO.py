import discord
import configparser
import discord_bot.Bot_GUIs.TODO_GUI as TODO_bot

class Todo:
    def __init__(self, client):
        self.client = client
        self.name = "TODO" # wichtig für die GUI
        self.bot_module = TODO_bot # wichtig für die GUI
        self.channel_list = []
        self.channel_file = "discord_bot/config/GUI_%s.cfg"%self.name
        self.todo = {}
        self.emoji = {
            "check" : "✅",
            "wichtig": "❗",
            "uncheck": "❌"
        }
        self.footer = "Es dürfen KEIN '|' oder ':' verwendet werden auserhalb der Befehlsstrucktur !!!"
        self.des = "!TODO start (!nur einmal verwenden)\n" \
                   "!TODO create <gruppe>\n" \
                   "!TODO drop <gruppe>\n" \
                   "!TODO add <gruppe>: <task>\n" \
                   "!TODO rm <gruppe>: <task_id>\n" \
                   "!TODO status <gruppe>: <task_id>: [check/wichtig/uncheck]\n"

        self.channel_update()

    def channel_update(self):
        self.todo = {}
        self.channel_list = []
        config = configparser.ConfigParser()
        config.sections()
        config.read(self.channel_file)

        for channel_id in config:
            if channel_id != "DEFAULT":
                self.channel_list.append(channel_id)
                self.todo[channel_id] = {}
                self.todo[channel_id]["message"] = config[channel_id]["message"]

    def use_on_message(self, message):
        if message.channel.id not in self.channel_list:
            return ""

        if message.content.startswith("!TODO start"):
            yield from self.todo_start(message)
        if message.content.startswith("!TODO stop"):
            yield from self.todo_stop(message)
        if message.content.startswith("!TODO create"):
            yield from self.todo_field(message, len("!TODO create"))
        if message.content.startswith("!TODO drop"):
            yield from self.todo_field(message,len("!TODO drop"), drop=True)

        if message.content.startswith("!TODO add"):
            yield from self.todo_add(message, len("!TODO add"))
        if message.content.startswith("!TODO rm"):
            yield from self.todo_rm(message, len("!TODO rm"))

        if message.content.startswith("!TODO status"):
            yield from self.todo_status(message, len("!TODO status"))

        if message.author.id != self.client.user.id:
            yield from self.client.delete_message(message)

        return ""

    def use_on_reaction_add(self, reaction, user):
        return ""
    def use_on_reaction_remove(self, reaction, user):
        return ""
    def todo_start(self, message):
        em = discord.Embed(
            title="TODO",
            description=self.des,
            colour=discord.Colour.dark_magenta()
        )
        em.set_footer(text=self.footer)
        msg = yield from self.client.send_message(message.channel, embed=em)

        config = configparser.ConfigParser()
        config.sections()
        config.read(self.channel_file)
        config[message.channel.id]["message"] = msg.id
        with open(self.channel_file, 'w') as configfile:
            config.write(configfile)

        self.todo[message.channel.id] = {}
        self.todo[message.channel.id]['message'] = msg.id

    def todo_stop(self, message):
        my_message_id = self.todo[message.channel.id]['message']
        if my_message_id == None or my_message_id == "None":
            return ""
        my_message = yield from self.client.get_message(message.channel, my_message_id)
        yield from self.client.delete_message(my_message)

        config = configparser.ConfigParser()
        config.sections()
        config.read(self.channel_file)
        config[message.channel.id]["message"] = "None"
        with open(self.channel_file, 'w') as configfile:
            config.write(configfile)

        self.channel_update()



    def todo_field(self, message, length, drop=False):
        my_message_id = self.todo[message.channel.id]['message']
        if my_message_id == None or my_message_id == "None":
            return ""
        my_message = yield from self.client.get_message(message.channel, my_message_id)

        try:
            embeds = my_message.embeds[0]["fields"]
        except: embeds = []

        create = message.content[length+1:]

        em = discord.Embed(
            title="TODO",
            description=self.des,
            colour=discord.Colour.dark_magenta()
        )
        em.set_footer(text=self.footer)
        exist = False
        for embed in embeds:
            if embed['name'] != create:
                em.add_field(name=embed['name'], value=embed['value'], inline=embed['inline'])
            else:
                if drop == False:
                    em.add_field(name=embed['name'], value=embed['value'], inline=embed['inline'])
                exist = True

        if embeds == [] or exist == False:
            em.add_field(name=create, value="-\n", inline=True)

        yield from self.client.edit_message(message=my_message, embed=em)
        return ""

    def todo_add(self, message, length):
        my_message_id = self.todo[message.channel.id]['message']
        if my_message_id == None or my_message_id == "None":
            return ""
        my_message = yield from self.client.get_message(message.channel, my_message_id)
        try:
            embeds = my_message.embeds[0]["fields"]
        except:
            embeds = []

        field = message.content[length + 1:].split(": ")

        em = discord.Embed(
            title="TODO",
            description=self.des,
            colour=discord.Colour.dark_magenta()
        )
        em.set_footer(text=self.footer)

        for embed in embeds:
            if embed['name'] == field[0]:
                task = embed['value']
                task += "\n"

                task += "\n%s\n"%field[1]
                task = task.split("\n")

                if task[0] == "-":
                    task.pop(0)


                text = ""
                for index, t in enumerate(task):
                    try:
                        text += "%s | %s | %s\n"%(index+1, t.split("|")[1],t.split("|")[2])
                    except:
                        if t != "":
                            text += "%s | %s | %s\n"%(index, t, self.emoji["uncheck"])
                em.add_field(name=embed['name'], value=text, inline=embed['inline'])
            else:
                em.add_field(name=embed['name'], value=embed['value'], inline=embed['inline'])

        yield from self.client.edit_message(message=my_message, embed=em)

    def todo_rm(self, message, length):
        my_message_id = self.todo[message.channel.id]['message']
        if my_message_id == None or my_message_id == "None":
            return ""
        my_message = yield from self.client.get_message(message.channel, my_message_id)
        try:
            embeds = my_message.embeds[0]["fields"]
        except:
            embeds = []

        field = message.content[length + 1:].split(": ")

        em = discord.Embed(
            title="TODO",
            description=self.des,
            colour=discord.Colour.dark_magenta()
        )
        em.set_footer(text=self.footer)

        for embed in embeds:
            if embed['name'] == field[0]:
                task = embed['value']

                task = task.split("\n")

                text = ""
                rm = 0
                for index, t in enumerate(task):
                    if index+1 == int(field[1]):
                        rm = -1
                        pass
                    else:
                        text += "%s | %s | %s\n"%(index+1+rm, t.split("|")[1],t.split("|")[2])

                if text == "":
                    text = "-"
                em.add_field(name=embed['name'], value=text, inline=embed['inline'])
            else:
                em.add_field(name=embed['name'], value=embed['value'], inline=embed['inline'])

        yield from self.client.edit_message(message=my_message, embed=em)

    def todo_status(self, message, length):
        my_message_id = self.todo[message.channel.id]['message']
        if my_message_id == None or my_message_id == "None":
            return ""
        my_message = yield from self.client.get_message(message.channel, my_message_id)
        try:
            embeds = my_message.embeds[0]["fields"]
        except:
            embeds = []

        field = message.content[length + 1:].split(": ")

        em = discord.Embed(
            title="TODO",
            description=self.des,
            colour=discord.Colour.dark_magenta()
        )
        em.set_footer(text=self.footer)

        for embed in embeds:
            if embed['name'] == field[0]:
                task = embed['value']

                task = task.split("\n")

                text = ""
                for index, t in enumerate(task):
                    if index+1 == int(field[1]):
                        text += "%s | %s | %s\n" % (index+1, t.split("|")[1], self.emoji[field[2]])
                    else:
                        text += "%s | %s | %s \n"%(index+1, t.split("|")[1], t.split("|")[2])

                if text == "":
                    text = "-"
                em.add_field(name=embed['name'], value=text, inline=embed['inline'])
            else:
                em.add_field(name=embed['name'], value=embed['value'], inline=embed['inline'])

        yield from self.client.edit_message(message=my_message, embed=em)

