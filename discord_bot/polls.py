import discord
import discord_bot.my_polls.poll as poll
import discord_bot.Bot_GUIs.polls_GUI as polls_GUI
import configparser

class Polls:
    def __init__(self, client):
        self.client = client
        self.name = "Umfragen" # wichtig für die GUI
        self.bot_module = polls_GUI # wichtig für die GUI
        self.roles_create = {}
        self.roles_del = {}
        self.emojis_more = ["1⃣", "2⃣", "3⃣", "4⃣", "5⃣"]
        self.emojis_one = ["✅", "❌"]
        self.emojis_inactive = "✋"
        self.anzeige_emoji = {
            "gut": ["⬜","◻","◽","◽"],
            "schlecht": ["⬛","◼","◾","▪"],
            "beides": ["🔳","🔲"]}
        self.umfragen = {}
        self.config_file = 'discord_bot/config/GUI_polls.cfg'

        self.roles_update()

    def roles_update(self):
        self.roles_create = {}
        self.roles_del = {}
        config = configparser.ConfigParser()
        config.sections()
        config.read(self.config_file)
        for server_id in config:
            if server_id != "DEFAULT":
                self.roles_create[server_id] = config[server_id]["role_to_create"]
                self.roles_del[server_id] = config[server_id]["role_to_delete"]

    def use_on_ready(self):
        my_server = [s.id for s in self.client.servers]
        for server in self.roles_create.keys():
            for i,s in enumerate(my_server):
                if server == s:
                    my_server.pop(i)

        if my_server != []:
            config = configparser.ConfigParser()
            config.sections()
            config.read(self.config_file)
        else: return ""

        for server in my_server:
            config[server] = {}
            config[server]["role_to_create"] = self.client.get_server(server).roles[0].id
            config[server]["role_to_delete"] = self.client.get_server(server).roles[0].id

        with open(self.config_file, 'w') as configfile:
            config.write(configfile)

    def use_on_message(self, message):
        if message.author.id == self.client.user.id:
            return ""
        if str(message.channel.type) == "private":
            return ""

        user_roles_ids = []
        for rol in message.author.roles:
            user_roles_ids.append(rol.id)
        if self.roles_create[message.server.id] not in user_roles_ids:
            return ""


        if message.content.startswith('!umfrage '):

            content = message.content[len('!umfrage '):]
            p = content.split('/')
            description = p[0]
            p.pop(0)

            if len(p) > 5 or len(p) == 1:
                return ""

            options = {}

            if len(p) == 0:
                options[self.emojis_one[0]] = {}
                options[self.emojis_one[0]]["name"] = "Ja"
                options[self.emojis_one[0]]["votes"] = []

                options[self.emojis_one[1]] = {}
                options[self.emojis_one[1]]["name"] = "nein"
                options[self.emojis_one[1]]["votes"] = []

                text = ""
                for i in range(0, 10):
                    text += "|%s|\n" % self.anzeige_emoji["schlecht"][0]
                em = discord.Embed(
                    title="Umfrage",
                    description="Teilnahme: 0\n" + description+"\n\n"+text,
                    colour=discord.Colour.dark_green()
                )

                msg = yield from self.client.send_message(message.channel, embed=em)

                self.umfragen[msg.id] = poll.poll(creator=message.author,
                                                  msg=msg,
                                                  beschreibung=description,
                                                  optionen=options)
                yield from self.client.add_reaction(msg, self.emojis_one[0])
                yield from self.client.add_reaction(msg, self.emojis_one[1])
            else:
                em = discord.Embed(
                    title="Umfrage",
                    description="Teilnahme: 0\n" + description,
                    colour=discord.Colour.dark_green()
                )

                for index, option in enumerate(p):
                    options[self.emojis_more[index]] = {}
                    options[self.emojis_more[index]]["name"] = option
                    options[self.emojis_more[index]]["votes"] = []

                    text = ""
                    for i in range(0,10):
                        text += "|%s|\n"%self.anzeige_emoji["schlecht"][0]
                    em.add_field(name=self.emojis_more[index] + " - " + option, value=text, inline=True)


                msg = yield from self.client.send_message(message.channel, embed=em)

                self.umfragen[msg.id] = poll.poll(creator=message.author,
                                              msg=msg,
                                              beschreibung=description,
                                              optionen=options)
                for i in range(0,len(options)):
                    yield from self.client.add_reaction(msg, self.emojis_more[i])
            yield from self.client.add_reaction(msg, self.emojis_inactive)
        return ""

    def use_on_reaction_add(self, reaction, user):
        if user == self.client.user:
            return ""

        if str(reaction.message.channel.type) == "private" or reaction.message.id not in self.umfragen.keys():
            return ""


        em = None
        if reaction.emoji in self.emojis_one:
            message_id = reaction.message.id
            emoji = reaction.emoji

            umfrage = self.umfragen[message_id]
            options = umfrage.optionen

            for option in options:
                if user.id in options[option]["votes"]:
                    index = options[option]["votes"].index(user.id)
                    self.umfragen[message_id].optionen[option]["votes"].pop(index)
            self.umfragen[message_id].optionen[emoji]["votes"].append(user.id)
            options = self.umfragen[message_id].optionen

            length_list = []
            length_max = 0
            for option in options:
                length_list.append(len(options[option]["votes"]))
                length_max = length_max + len(options[option]["votes"])



            for index, length in enumerate(length_list):
                if length == 0:
                    percent = 0
                else:
                    percent = (length / length_max) * 10

                text = ""
                now = 10

                for i in range(0, 10):

                    if now -1 <= percent:
                        if now >= percent:
                            wert = percent * 10
                            if int(wert) == 100:
                                text += "|%s| <-- %s%s\n" % (self.anzeige_emoji["gut"][0], round(wert, 2), "%")
                            elif (percent % 1) > 0.5:
                                text += "|%s| <-- %s%s\n" % (self.anzeige_emoji["beides"][1], round(wert, 2), "%")
                            else:
                                text += "|%s|\n" % self.anzeige_emoji["schlecht"][0]
                        else:
                            text += "|%s|\n" % self.anzeige_emoji["gut"][0]
                    else:
                        text += "|%s|\n" % self.anzeige_emoji["schlecht"][0]
                    now = now - 1
                if index == 0:
                    anzeige = text
            em = discord.Embed(
                title="Umfrage",
                description="Teilnehmer: %s\n" % (length_max) + umfrage.beschreibung+"\n\n"+anzeige,
                colour=discord.Colour.dark_green()
            )

        if reaction.emoji in self.emojis_more:
            message_id = reaction.message.id
            emoji = reaction.emoji

            umfrage = self.umfragen[message_id]
            options = umfrage.optionen

            for option in options:
                if user.id in options[option]["votes"]:
                    index = options[option]["votes"].index(user.id)
                    self.umfragen[message_id].optionen[option]["votes"].pop(index)
            self.umfragen[message_id].optionen[emoji]["votes"].append(user.id)
            options =self.umfragen[message_id].optionen

            length_list = []
            length_max = 0
            for option in options:
                length_list.append(len(options[option]["votes"]))
                length_max = length_max + len(options[option]["votes"])

            em = discord.Embed(
                title="Umfrage",
                description="Teilnehmer: %s\n"%(length_max) + umfrage.beschreibung,
                colour=discord.Colour.dark_green()
            )
            for index, length in enumerate(length_list):
                if length == 0:
                    percent = 0
                else:
                    percent = (length /length_max) * 10

                text = ""
                now = 10
                for i in range(0, 10):

                    if now <= percent:
                        if now+1 > percent:
                            wert = percent * 10

                            if int(wert) == 100:
                                text += "|%s| <-- %s%s\n" % (self.anzeige_emoji["gut"][0], round(wert, 2), "%")
                            elif (percent % 1) > 0.5:
                                text += "|%s| <-- %s%s\n"%(self.anzeige_emoji["gut"][0],round(wert, 2), "%")
                            else:
                                text += "|%s| <-- %s%s\n" % (self.anzeige_emoji["beides"][1], round(wert, 2), "%")
                        else:
                            text += "|%s|\n"%self.anzeige_emoji["gut"][0]
                    else:
                        text += "|%s|\n"%self.anzeige_emoji["schlecht"][0]
                    now = now -1
                em.add_field(name=self.emojis_more[index] + " - " + options[self.emojis_more[index]]["name"], value=text, inline=True)

        if reaction.emoji == self.emojis_inactive:
            user_roles_ids = []
            for rol in user.roles:
                user_roles_ids.append(rol.id)

            if self.umfragen[reaction.message.id].creator.id == user.id or self.roles_del[reaction.message.server.id] in user_roles_ids:
                self.umfragen.pop(reaction.message.id)

        if em != None:
            yield from self.client.edit_message(message=reaction.message, embed=em)

            yield from self.client.remove_reaction(reaction.message, reaction.emoji, user)
        return ""
    def use_on_reaction_remove(self, reaction, user):
        return ""