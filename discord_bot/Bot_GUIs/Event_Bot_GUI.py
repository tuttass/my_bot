import configparser
import discord

def reaction_handler(self, reaction, title):
    if (title == "Channel"):
        yield from reaction_Channel(self, reaction)
    if (title[:10] == "Extras -->"):
        yield from reaction_Extras(self, reaction)
    if (title[:12] == "Chose a new "):
        yield from reaction_choice(self, reaction)

def First(self,channel_id):
    bot = self.user[channel_id]['bots']
    em = Channel(self, channel_id, bot)
    return em

def Channel(self, channel_id, Bot):
    self.user[channel_id]['channel'] = []
    des = ""
    head = 0
    config = configparser.ConfigParser()
    config.sections()
    config.read('discord_bot/config/GUI_%s.cfg' % Bot.name)
    index = 0
    for my_event in config:
        if my_event != "DEFAULT":
            ch = self.client.get_channel(my_event)

            if ch.server.id == self.user[channel_id]["server"].id:
                self.user[channel_id]['channel'].append(ch)
                des += "%s| %s --> %s\n\n" % (self.my_GUI['emoji'][index + 1], str(ch.name), str(ch.id))
                index = index + 1

                for event in Bot.events['list'].keys():
                    if Bot.events['list'][event].msg.channel.id == ch.id:
                        head = head +1

    em = discord.Embed(
        title="%s: Channel"%self.user[channel_id]['bots'].name,
        description="> %s aktive Events\n\n"%head + des,
        colour=discord.Colour.dark_magenta()
    )

    options = "%s| %s" % (self.my_GUI['emoji'][10], "Bot in einem neuen Channel hinzufügen")
    em.add_field(name='Mögliche aktionen:', value=options, inline=False)

    footer = "Server: %s --> Wähle einen Channel in denen Befehle ausgeführt werden können"%self.user[channel_id]["server"].name
    em.set_footer(text=footer)

    return em


def reaction_Channel(self, reaction):
    channel_id = reaction.message.channel.id
    for i in range(9):
        try:
            if reaction.emoji == self.my_GUI['emoji'][i + 1]:
                self.user[channel_id]['channel'] = self.user[channel_id]['channel'][i]
                yield from self.client.edit_message(message=reaction.message,
                                                    embed=Extras(self, channel_id, self.user[channel_id]['channel']))
        except:
            pass
    if reaction.emoji == self.my_GUI['emoji'][10]:
        self.user[channel_id]['extras'] = ["channel",]
        yield from self.client.edit_message(message=reaction.message,
                                            embed=choice(self, channel_id, 0))
    if reaction.emoji == self.my_GUI['emoji'][13]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=self.Bots(channel_id))


def Extras(self, channel_id, channel):
    self.user[channel_id]['extras'] = []
    des = ""
    config = configparser.ConfigParser()
    config.sections()
    config.read('discord_bot/config/GUI_%s.cfg' % self.user[channel_id]['bots'].name)

    for my_event in config:
        if my_event != "DEFAULT":
            if my_event == channel.id:
                for index, extra in enumerate(config[my_event]):
                    self.user[channel_id]['extras'].append([extra, config[my_event][extra]])
                    if extra == "rolle":
                        for server in self.client.servers:
                            for rol in server.roles:
                                if rol.id == config[my_event][extra]:
                                    des += "%s| %s --> %s\n\n" % (self.my_GUI['emoji'][index + 1], str(extra), rol.name)
                    else:
                        ch = self.client.get_channel(config[my_event][extra])
                        des += "%s| %s --> %s\n\n" % (self.my_GUI['emoji'][index + 1], str(extra), ch.name)

    options = "%s| %s" % (self.my_GUI['emoji'][10], "Bot aus Channel %s entfernen" % channel.name)

    em = discord.Embed(
        title="%s: Extras --> %s" %(self.user[channel_id]['bots'].name,channel.name),
        description=des,
        colour=discord.Colour.dark_magenta()
    )

    em.add_field(name='Mögliche aktionen:', value=options, inline=False)
    footer = "Server: %s --> Wähle was du ändern möchtest"%self.user[channel_id]["server"].name
    em.set_footer(text=footer)
    return em


def reaction_Extras(self, reaction):
    channel_id = reaction.message.channel.id
    for i in range(8):
        try:
            if reaction.emoji == self.my_GUI['emoji'][i + 1]:
                self.user[channel_id]['extras'] = self.user[channel_id]['extras'][i]
                yield from self.client.edit_message(message=reaction.message, embed=choice(self, channel_id, 0))
        except:
            pass
    if reaction.emoji == self.my_GUI['emoji'][9]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=Channel(self, channel_id, self.user[channel_id]['bots']))

    if reaction.emoji == self.my_GUI['emoji'][10]:
        remove_channel(self.user[channel_id]["bots"],self.user[channel_id]["channel"].id)

        yield from self.client.edit_message(message=reaction.message,
                                            embed=Channel(self, channel_id, self.user[channel_id]['bots']))
    if reaction.emoji == self.my_GUI['emoji'][13]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=Channel(self, channel_id, self.user[channel_id]['bots']))

def choice(self, channel_id, side):
    des = ""
    index = 0
    Name = None

    if side == 0:
        self.user[channel_id]['new'] = []

        if self.user[channel_id]['extras'][0] == "rolle":
            Name = "rolle"
            footer = "Wähle eine Rolle"
            for s in self.client.servers:
                if s.id == self.user[channel_id]['server'].id:
                    for rol in s.roles:
                        if index <= 9:
                            des += "%s| %s --> %s \n\n" % (self.my_GUI['emoji'][index + 1], rol.name, rol.id)
                        self.user[channel_id]['new'].append(rol)
                        index = index + 1
        else:
            Name = "channel"
            footer = "Wähle einen channel mit der rolle 'Bot_'"
            for s in self.client.servers:
                if s.id == self.user[channel_id]['server'].id:
                    for c in s.channels:
                        for rol in c.changed_roles:
                            if rol.name == "Bot_":
                                if index <= 9:
                                    des += "%s| %s --> %s \n\n" % (self.my_GUI['emoji'][index + 1], c.name, c.id)
                                self.user[channel_id]['new'].append(c)
                                index = index + 1
    else:
        if self.user[channel_id]['extras'][0] == "rolle":
            Name = "rolle"
            footer = "Wähle eine Rolle"
        else:
            Name = "channel"
            footer = "Wähle einen channel mit der rolle 'BOT_'"
        for item in self.user[channel_id]['new'][side * 10:]:
            if index <= 9:
                des += "%s| %s --> %s \n\n" % (self.my_GUI['emoji'][index + 1], item.name, item.id)
            index = index + 1

    em = discord.Embed(
        title="%s: Chose a new %s [%s / %s]" % (self.user[channel_id]['bots'].name,Name, side + 1, int(len(self.user[channel_id]['new']) / 10) + 1),
        description=des,
        colour=discord.Colour.dark_gold()
    )
    self.user[channel_id]["side"]=side


    em.set_footer(text="Server: %s --> "%(self.user[channel_id]["server"].name)+footer)
    return em


def reaction_choice(self, reaction):

    channel_id = reaction.message.channel.id
    side = self.user[channel_id]["side"]

    for i in range(10):
        try:
            if reaction.emoji == self.my_GUI['emoji'][i + 1]:

                if type(self.user[channel_id]['channel']) == list:
                    add_channel(self.user[channel_id]['bots'], self.user[channel_id]['new'][i + (side * 10)].id,
                                self.client)
                    self.user[channel_id]["channel"] = self.user[channel_id]['new'][i + (side * 10)]
                    yield from self.client.edit_message(message=reaction.message,
                                                        embed=Channel(self, channel_id, self.user[channel_id]['bots']))
                else:
                    change_property(self.user[channel_id]['bots'], self.user[channel_id]['channel'].id,self.user[channel_id]['extras'][0],
                                         self.user[channel_id]['new'][i + (side * 10)].id)
                    yield from self.client.edit_message(message=reaction.message,embed=Extras(self, channel_id,self.user[channel_id]['channel']))
        except:
            pass

    if reaction.emoji == self.my_GUI['emoji'][11]:
        if side - 1 < 0:
            side = 0
        else:
            side = side - 1
        yield from self.client.edit_message(message=reaction.message, embed=choice(self,channel_id, side))
    if reaction.emoji == self.my_GUI['emoji'][12]:
        if side + 1 >= int(len(self.user[channel_id]['new']) / 10) + 1:
            side = int(len(self.user[channel_id]['new']) / 10)
        else:
            side = side + 1
        yield from self.client.edit_message(message=reaction.message, embed=choice(self, channel_id, side))
    if reaction.emoji == self.my_GUI['emoji'][13]:
        if type(self.user[channel_id]['channel']) == list:
            yield from self.client.edit_message(message=reaction.message,
                                                embed=Channel(self, channel_id, self.user[channel_id]['bots']))
        else:
            yield from self.client.edit_message(message=reaction.message,
                                            embed=Extras(self, channel_id,self.user[channel_id]['channel']))

def add_channel(Bot, channel_id, client):
    config = configparser.ConfigParser()
    config.sections()
    config.read('discord_bot/config/GUI_%s.cfg' % Bot.name)

    rol_id = channel_id
    index = 0
    for server in client.servers:
        for rol in server.roles:
            if index == 0:
                rol_id=rol.id
                index = index +1

    config[channel_id] = {}
    config[channel_id]["channel_erstellt"] = channel_id
    config[channel_id]["channel_geloescht"] = channel_id
    config[channel_id]["rolle"] = rol_id
    with open('discord_bot/config/GUI_%s.cfg' % Bot.name, 'w') as configfile:
        config.write(configfile)
    Bot.channel_update()

def remove_channel(Bot, channel_id):
    config = configparser.ConfigParser()
    config.sections()
    config.read('discord_bot/config/GUI_%s.cfg' % Bot.name)

    config.pop(channel_id)
    with open('discord_bot/config/GUI_%s.cfg' % Bot.name, 'w') as configfile:
        config.write(configfile)
    Bot.channel_update()

def change_property(Bot, channel_id, propertie, new):
    config = configparser.ConfigParser()
    config.sections()
    config.read('discord_bot/config/GUI_%s.cfg' % Bot.name)

    config[channel_id][propertie] = str(new)
    with open('discord_bot/config/GUI_%s.cfg' % Bot.name, 'w') as configfile:
        config.write(configfile)
    Bot.channel_update()