import discord
import configparser
import time
import discord_bot.Bot_GUIs.member_activitaet_GUI as aktivitaet

class Log:
    def __init__(self, client):
        self.client = client
        self.name = "Aktivitäten"  # wichtig für die GUI
        self.bot_module = aktivitaet  # wichtig für die GUI
        self.channel_join_path = 'discord_bot/my_member_activitaet/channel_join.cfg'
        self.statistik_path = 'discord_bot/my_member_activitaet/statistik.cfg'
    def use_on_message(self, message):
        return ""

    def use_on_reaction_add(self, reaction, user):
        return ""

    def use_on_reaction_remove(self, reaction, user):
        return ""

    def use_on_voice_state_update(self, member):

        config_1 = configparser.ConfigParser()
        config_1.sections()
        config_1.read(self.channel_join_path)

        if member.server.id not in config_1.keys():
            config_1[member.server.id]= {}

        for server in config_1:
            if server != "DEFAULT":
                if server == member.server.id:
                    if member.nick:
                        config_1[member.server.id][member.id] = member.nick + ";" +str(time.time())
                    else:
                        config_1[member.server.id][member.id] = member.name + ";" + str(time.time())

        with open(self.channel_join_path, 'w') as configfile:
            config_1.write(configfile)



        config_2 = configparser.ConfigParser()
        config_2.sections()
        config_2.read(self.statistik_path)

        if member.server.id not in config_2.keys():
            config_2[member.server.id] = {}


        for server in config_2:
            if server != "DEFAULT":
                if server == member.server.id:
                    if "insgesammt" not in config_2[server]:
                        config_2[server]["insgesammt"] = "0"
                    else: config_2[server]["insgesammt"] = str(int(config_2[server]["insgesammt"]) + 1)

                    monat = time.strftime("%Y;%m")

                    if monat not in config_2[server]:
                        config_2[server][monat] = "0"
                    else:
                        config_2[server][monat] = str(int(config_2[server][monat]) + 1)

        with open(self.statistik_path, 'w') as configfile:
            config_2.write(configfile)
