import discord
import discord_bot.Bot_GUIs.Game_Abo_GUI as Game_Abo
import configparser

class Game_abos:
    def __init__(self, client):
        self.client = client
        self.name = "Game_Abo" # wichtig für die GUI
        self.bot_module = Game_Abo # wichtig für die GUI
        self.messages = {}
        self.master = '489834807983996949' #id der Rolle zum erstellen
        self.emojies = ["✅", "❌"]
        self.file= 'discord_bot/my_Game_abos/Abos.cfg'
        self.emojie_del = "☠"
        self.del_rol = {}
        self.config_file = "discord_bot/config/GUI_game_abo.cfg"

        self.update_roles()

    def update_roles(self):
        self.del_rol = {}
        config = configparser.ConfigParser()
        config.sections()
        config.read(self.config_file)
        for my_event in config:
            if my_event != "DEFAULT":
                 self.del_rol[my_event] = {}
                 self.del_rol[my_event]['role_to_delete'] = config[my_event]['role_to_delete']

    def use_on_ready(self):

        yield from self.readInFile()
        yield from self.writeIniFile()

    def use_on_message(self, message):
        if message.author.id == self.client.user.id:
            return ""
        if str(message.channel.type) == "private":
            return ""
        user_roles_ids = []
        for rol in message.author.roles:
            user_roles_ids.append(rol.id)
        if self.master not in user_roles_ids:
            return ""

        if message.content.startswith("!add Game "):
            content = message.content[len("!add Game "):]
            des1 = content.split("<")[0]
            des2 = content.split(">")[1]

            crol = content.replace(des1, "").replace(des2, "")
            id = crol[3:-1]
            rol = discord.utils.get(message.author.server.roles, id=id)

            des = des1 + rol.name + des2

            msg = yield from self.client.send_message(message.channel, embed=discord.Embed(title="Error",
                                                                                           description=" ! Irgend etwas ist schief gelaufen !!",
                                                                                           colour=discord.Colour.blue()))
            #!add Game was auch @Anthem immer du willst
            self.messages[msg.id] = {}
            self.messages[msg.id]["msg"] = msg
            self.messages[msg.id]["rol"] = rol
            self.messages[msg.id]["des"] = des

            yield from self.client.edit_message(msg, embed=discord.Embed(title="%s"%rol.name,
                                                           description=des,
                                                           colour=discord.Colour.dark_green()))

            for e in self.emojies:
                yield from self.client.add_reaction(msg, e)

            yield from self.client.delete_message(message)
            self.writeIniFile()
        return ""

    def use_on_reaction_add(self, reaction, user):
        if reaction.message.id not in self.messages.keys() or user == self.client.user:
            return ""

        if reaction.emoji == self.emojies[0]:
            yield from self.client.add_roles(user, self.messages[reaction.message.id]["rol"])

        if reaction.emoji == self.emojies[1]:
            yield from self.client.remove_roles(user, self.messages[reaction.message.id]["rol"])

        yield from self.client.remove_reaction(reaction.message, reaction.emoji, user)

        if reaction.emoji == self.emojie_del:
            user_roles_ids = []
            for rol in user.roles:
                user_roles_ids.append(rol.id)

            if self.del_rol[reaction.message.server.id]['role_to_delete'] in user_roles_ids:
                self.messages.pop(reaction.message.id)
                self.writeIniFile()
                yield from self.client.delete_message(reaction.message)

        return ""

    def use_on_reaction_remove(self, reaction, user):
        return ""

    def writeIniFile(self):
        config = configparser.ConfigParser()
        config["abos"] = {}
        for main_key in self.messages.keys():
            config["abos"][main_key]=str(self.messages[main_key]["rol"].id)+","+str(self.messages[main_key]["msg"].channel.id)+","+ self.messages[main_key]["des"]
        with open(self.file, 'w') as configfile:
            config.write(configfile)

    def readInFile(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read(self.file)

        for msg_id in config["abos"].keys():
            rol_id, ch_id, des = config["abos"][msg_id].split(",")
            channel =  self.client.get_channel(ch_id)
            message = yield from self.client.get_message(channel, msg_id)
            rol = discord.utils.get(channel.server.roles, id=rol_id)


            msg = yield from self.client.send_message(channel, embed=discord.Embed(title="Error",
                                                                                    description=" ! Irgend etwas ist schief gelaufen !!",
                                                                                    colour=discord.Colour.blue()))

            self.messages[msg.id] = {}
            self.messages[msg.id]["msg"] = msg
            self.messages[msg.id]["rol"] = rol
            self.messages[msg.id]["des"] = des

            yield from self.client.edit_message(msg, embed=discord.Embed(title="%s" % rol.name,
                                                                         description=des,
                                                                         colour=discord.Colour.dark_green()))
            for e in self.emojies:
                yield from self.client.add_reaction(msg, e)

            yield from self.client.delete_message(message)