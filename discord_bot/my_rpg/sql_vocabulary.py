OBJECT = {
    'quality': {
        'ordinary':1,
        1:'ordinary',

        'rare': 2,
        2:'rare',

        'legendary':3,
        3:'legendary',
    },
    'status': {
        "normal": 1,
        1:"normal",
    },
    'type': {
        'weapon':1,
        1:'weapon',

        'armor':2,
        2:'armor'
    }
}

WEAPON = {

    'type': {
        'meele': 1,
        1: 'meele',

        'range': 2,
        2: 'range',

        'magic': 3,
        3: 'magic',
    },


    'effect': {
        1: "+ 10 damage while on fire",
    },

    'element': {
        'None':0,
        0:None,

        'fire': 1,
        1: 'fire',

        'ice': 2,
        2: 'ice',

        'poison':3,
        3:'poison',

        'electricity':4,
        4:'electricity'
    }
}

ARMOR = {
    'type': {
        1: 'heavy',
        'heavy': 1,

        2: 'medium',
        'medium':2,

        3: 'light',
        'light':3
    },

    'bodypart': {
        1: 'head',
        'head':1,

        2: 'chest',
        'chest':2,

        3: 'poor',
        'poor':3,

        4: 'legs',
        'legs':4,

        5: 'shoes',
        'shoes':5
    }
}