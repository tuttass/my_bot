from discord_bot.my_rpg.objects import object

class stats:
    def __init__(self, name, life_points, mana, xp=0, level=0):
        self.__name = name
        self.__max_life_points = life_points
        self.__max_mana_points = mana

        self.__current_mana_points = mana
        self.__current_life_points = life_points

    @property
    def get_name(self):
        return self.__name
    def set_name(self, name):
        self.__name = name

    @property
    def get_max_lifePoints(self):
        return self.__max_life_points
    def set_max_lifePoints(self, lifePoints):
        self.__max_life_points = lifePoints

    @property
    def get_current_lifePoints(self):
        return self.__current_life_points
    def update_current_lifePoints(self, value):
        self.__current_life_points = self.__current_life_points + int(value)
        if self.__current_life_points > self.__max_life_points:
            self.__current_life_points = self.__max_life_points
        if self.__current_life_points < 0:
            self.__current_life_points=0

    @property
    def get_max_mana(self):
        return self.__max_mana_points
    def set_max_mana(self, mana):
        self.__max_mana_points = mana

    @property
    def get_current_mana(self):
        return self.__current_life_points
    def update_current_mana(self, value):
        self.__current_mana_points = self.__current_mana_points + int(value)
        if self.__current_mana_points > self.__max_mana_points:
            self.__current_mana_points = self.__max_mana_points
        if self.__current_mana_points < 0:
            self.__current_mana_points = 0




class player(stats):
    def __init__(self, name, life_points, mana, species="Mensch"):
        stats.__init__(self, name=name, life_points=life_points, mana=mana)
        self.__xp = 0
        self.__level = 0
        self.__max_level = 10

        self.__discord_user = None
        self.__discord_message = None

        self.__species = species
        self.__max_attribute_level = 10
        self.__attributes = {
            'strength': 0,
            'inteligence': 0,
            'endurance': 0,
            'initiative': 0,
            'loadability': 0
        }
        self.__equipment = {
            'weapon': None,
            'hand': None,
            'head': None,
            'chest': None,
            'poor': None,
            'legs': None,
            'shoes': None,
        }
        self.__inventory_space = 10
        self.__inventory = []

    @property
    def get_current_XP(self):
        return self.__xp
    def update_current_XP(self, value):
        if self.__level != self.__max_level:
            self.__xp = self.__xp + int(value)

    @property
    def get_discord_user(self):
        return self.__discord_user
    def set_discord_user(self, user):
        self.__discord_user = user

    @property
    def get_discord_message(self):
        return self.__discord_message
    def set_discord_message(self, message):
        self.__discord_message = message

    @property
    def get_species(self):
        return self.__species
    def set_discord_message(self, species):
        self.__species = species

    @property
    def get_max_attribute_level(self):
        return self.__max_attribute_level
    @property
    def get_attributes(self):
        return self.__attributes
    def update_attribute(self, attribute, value):
        if (self.__attributes[attribute] + value <= self.__max_attribute_level and self.__attributes[attribute] + value >= 0):
            self.__attributes[attribute] += value
        else:
            return False

    @property
    def get_equipment(self):
        return self.__equipment
    def change_equipment(self, object, value):
        pass
    def add_to_equipment(self, obj):
        if obj.get_type == "weapon":
            self.__equipment[obj.get_type] = obj
        else:
            self.__equipment[obj.get_armor_bodypart] = obj

    @property
    def get_inventory(self):
        return self.__inventory
    def add_to_inventory(self, obj):
        if len(self.__inventory) < self.get_inventory_space:
            self.__inventory.append(obj)
    def delete_item_from_inventory(self, index):
        self.__inventory.pop(index)
    @property
    def get_inventory_space(self):
        return self.__inventory_space + self.__attributes['loadability']

class enemy(stats):
    def __init__(self, name, life_points, mana=0, species="org"):
        stats.__init__(self, name=name, life_points=life_points, mana=mana)

if __name__ == "__main__":
    play = player("testname",10,10)
    print(play.get_current_lifePoints)
    play.add_object()
    print(play.get_equipment)
    #o = object(obj_type="armor", db_name="../rpg.db")
    #self.__equipment[o.get_armor_type] = o