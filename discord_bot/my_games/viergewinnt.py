from copy import deepcopy
import time
import random


class Game:
    def __init__(self, p, p2):
        self.__len_column = 7
        self.__len_rows = 6
        self.__max_stones = self.__len_column * self.__len_rows

        self.__player1 = p
        self.__player2 = p2

        self.__current_player = p
        self.__started_player = self.__current_player
        self.__history = []

        self.__count_column = []
        self.__field = []
        self.__setup()
        self.__input = {0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6}

    def __setup(self):
        self.__field = [[0 for x in range(self.__len_column)] for x in range(self.__len_rows)]
        self.__count_column = [0 for x in range(self.__len_column)]

    def __printNewLine(self):
        """ Helper Method for printField, prints a new-line with corresponding headers,.. """
        line = "+"
        for i in range(0, self.__len_column):
            line += '---+'
        self.text += '\n' + line

    def __getTranslation(self, content):

        if content == 0:
            return " "
        elif content == self.__player1.id:
            return "X"
        elif content == self.__player2.id:
            return "O"

    def printField(self):
        self.text = ""
        """ Print the field human-readable in stdout """
        #print("\nVIER GEWINNT ")
        self.__printNewLine()
        for row in reversed(self.__field):
            self.text += '\n' + '| '
            for column in row:
                self.text += str(self.__getTranslation(column)) + ' | '
            self.__printNewLine()
        self.text += "\n "
        for i in range(self.__len_column):
            self.text += " %s  "%str(i +1)
        return self.text

    def add_stone(self, col):
        c = int(col)
        try:
            row = self.__count_column[c]
        except:
            return "spalte gibt es nicht"

        if row < self.__len_rows:
            self.__field[row][c] = self.__current_player.id
            self.__count_column[c] = row + 1

            if self.__current_player == self.__player1:
                self.__current_player = self.__player2
            else:
                self.__current_player = self.__player1

            self.__history.append(col)
            return True
        else:
            return "spalte voll"

    def checkWin(self, in_row=4):
        if self.__max_stones == len(self.__history):
            return None
        # Check horizontally

        for col in range(self.__len_column - (in_row - 1)):
            for row in range(self.__len_rows):
                if in_row == 4:
                    if (self.__field[row][col] == self.__field[row][col + 1] == self.__field[row][col + 2] ==
                            self.__field[row][col + 3] != 0):
                        return [self.__field[row][col], "horizontally"]

        # Check vertically
        for row in range(self.__len_rows - (in_row - 1)):
            for col in range(self.__len_column):
                if in_row == 4:
                    if (self.__field[row][col] == self.__field[row + 1][col] == self.__field[row + 2][col] ==
                            self.__field[row + 3][col] != 0):
                        return [self.__field[row][col], "vertically"]

        # Skip diagonal checks if column count is less than 4
        if (self.__len_column < in_row):
            return None

        # Check up-diagonally
        for col in range(self.__len_column - (in_row - 1)):
            for row in range(self.__len_rows - (in_row - 1)):
                if in_row == 4:
                    if (self.__field[row][col] == self.__field[row + 1][col + 1] == self.__field[row + 2][col + 2] ==
                            self.__field[row + 3][col + 3] != 0):
                        return [self.__field[row][col], "up-diagonally"]

        # Check down-diagonally
        for col in range(in_row - 1, self.__len_column):
            for row in range(self.__len_rows - (in_row - 1)):
                if in_row == 4:
                    if (self.__field[row][col] == self.__field[row + 1][col - 1] == self.__field[row + 2][col - 2] ==
                            self.__field[row + 3][col - 3] != 0):
                        return [self.__field[row][col], "down-diagonally"]

        return None

    def check_next_round(self, runde=0, befor=None):
        f = deepcopy(self.__field)
        cc = deepcopy(self.__count_column)
        current_player = deepcopy(self.__current_player)

        feld = []
        for mein_feld in range(0, self.__len_column):
            feld.append(deepcopy(self.__field))

        for i in range(0, len(feld)):
            self.__field = feld[i]
            if self.__len_rows - 1 >= cc[i]:
                try:
                    self.add_stone(i)
                    # self.printField()
                    # time.sleep(0.1)
                except:
                    return self.add_stone(i)

                if self.checkWin() != None:
                    if befor == None or befor != i:
                        #print(str(i) + " <-- hier")
                        return i

                    if befor == i:
                        if len(self.get_history) == 42:
                            return i
                        #print(str(i) + " <-- Weg")
                        try:
                            self.__input.pop(i, None)
                        except:
                            pass

                if runde == 0:
                    #print("--> " + str(i))
                    win = self.check_next_round(runde=runde + 1, befor=i)
                    # print("win: " + str(win))
                    if win != None:
                        return win

            # reset des spielfeldes
            self.__current_player = current_player
            self.__count_column = deepcopy(cc)
            self.__field = f

        # macht nichts kaputt bei normalen gebrauch unnötig
        self.__current_player = current_player
        self.__count_column = deepcopy(cc)
        self.__field = f

        return None

    @property
    def get_field(self):
        return self.__field

    @property
    def get_player1(self):
        return self.__player1

    @property
    def get_player2(self):
        return self.__player2

    @property
    def get_current_player(self):
        return self.__current_player

    @property
    def get_column_counter(self):
        return self.__count_column

    @property
    def get_history(self):
        return self.__history

    @property
    def get_started_player(self):
        return self.__started_player

    @property
    def get_copy(self):
        return deepcopy(self)

    @property
    def get_input(self):
        return self.__input


class player:
    def __init__(self, name="player", ki=False):
        self.__name = name
        self.__id = random.randint(0, 1000000)
        self.__ki = False
        if ki == True:
            self.__ki = True

    def eingabe(self, game):
        if self.__ki == False:
            return input("Spalte: ")

        check = game.check_next_round()
        if check == None:
            enter = random.choice(list(game.get_input.keys()))
            #print("choice: " + str(enter))
            return enter
        return check

    @property
    def get_id(self):
        return self.__id

    @property
    def get_name(self):
        return self.__name

    @property
    def get_ki(self):
        return self.__ki


if "__main__" == __name__:
    p1 = player(ki=True)
    p2 = player(ki=True)
    a = Game(p1.get_id, p2.get_id)
    while True:
        player_id = a.get_current_player
        if player_id == p1.get_id:
            eingabe = p1.eingabe(a.get_copy)
        else:
            eingabe = p2.eingabe(a.get_copy)

        a.add_stone(eingabe)
        win = a.checkWin()
        # time.sleep(0.2)
        a.printField()

        if len(a.get_history) == 42:
            print("unentschieden")
            break;
        if win != None:
            # a = Game(p1, p2)
            print(a.get_history)
            print("Gewonnen hat: " + str(a.get_current_player))
            break;
