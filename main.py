import discord
import SECRETS
import asyncio as asyncio
from beautifultable import BeautifulTable
import discord_bot
import deepdiff
import time

# local flo 512007723047452682 can Nicht vorhanden
# live flo flo 496353613426196530 can 518537029546672148
wichtige_channel = ["512007723047452682", "518537029546672148"]
# Benachichtigung
wichtige_user = ["263814781381640194",]
# local 540648967629701126
# live
audit_log = "540648967629701126"

class Flo_Z_Bot:

    def __init__(self):
        self.apps = []

    def add_app(self, app):
        self.apps.append(app)

    def use_as_on_ready(self):

        i = 0
        for s in client.servers:
            if s.id == "221722444103286784":
                member = s.members
                for m in member:
                    pass
            print("\n----------------Server_Rollen---------------------")
            print("Server: %s \nId: %s"%(s.name, s.id))
            table = BeautifulTable()
            table.column_headers = ["Name", "Id"]
            for rol in s.roles:
                table.append_row([rol.name, rol.id])

            print(table)
        for app in self.apps:
            try:
                yield from app.use_on_ready()
            except: pass
        print("ready")
        yield from client.change_presence(game=discord.Game(name="GoW "))

    def use_as_on_message(self, message):
        for app in self.apps:
            yield from app.use_on_message(message)

    def use_as_on_reaction_add(self, reaction, user):
        for app in self.apps:
            yield from app.use_on_reaction_add(reaction, user)

    def use_as_on_reaction_remove(self, reaction, user):
        for app in self.apps:
            yield from app.use_on_reaction_remove(reaction, user)

    def use_as_on_voice_state_update(self, member_before, member_after):
        for app in self.apps:
            try:
                yield from app.use_on_voice_state_update(member_before)
            except AttributeError:
                pass

        return ""

    def use_as_on_resume(self):
        # Called when the client has resumed a session.
        pass

    def use_as_on_error(self, event, *args, **kwargs):
        print("-----------ERROR------------")
        print(event)
        print(args)
        print(kwargs)
        # Usually when an event raises an uncaught exception, a traceback is printed to stderr and the exception is ignored.
        pass

    def use_as_on_socket_raw_receive(self, msg):
        #Called whenever a message is received from the websocket, before it’s processed.This event is always dispatched when a message is received and the passed data is not processed in any way.
        pass

    def use_as_on_socket_raw_send(self, payload):
        #Called whenever a send operation is done on the websocket before the message is sent. The passed parameter is the message that is to sent to the websocket.
        pass

    def use_as_on_message_delete(self, message):
        # Called when a message is deleted. If the message is not found in the Client.messages cache, then these events will not be called.
        pass

    def use_as_on_message_edit(self, before, after):
        # Called when a message receives an update event. If the message is not found in the Client.messages cache, then these events will not be called.
        pass

    def use_as_on_reaction_clear(self, message, reactions):
        # Called when a message has all its reactions removed from it. Similar to on_message_edit, if the message is not found in the Client.messages cache, then this event will not be called.
        pass

    def use_as_on_channel_delete(self, channel):
        # Called whenever a channel is removed or added from a server.
        pass

    def use_as_on_channel_create(self, channel):
        # Called whenever a channel is removed or added from a server.
        pass

    def use_as_on_channel_update(self, before, after):
        #print(dir(before))
        aenderung=deepdiff.DeepDiff(before,after)
        text = None
        if "values_changed" in aenderung:
            for key in aenderung["values_changed"]:
                this_aenderung = aenderung["values_changed"][key]
                text= "Channel %s wurde verändert. %s wurde von %s auf %s geändert."%(after.name, key.split(".")[1], this_aenderung["old_value"], this_aenderung["new_value"] )
        elif "type_changes" in aenderung:
            for key in aenderung["type_changes"]:
                this_aenderung = aenderung["type_changes"][key]
                text = "Channel %s wurde verändert. %s wurde von %s auf %s geändert." % ( after.name, key.split(".")[1], this_aenderung["old_value"], this_aenderung["new_value"])
        elif "iterable_item_removed" in aenderung:
            roles = {}

            for rol in before.server.roles:
                roles[rol.id] = rol.name
            for key in aenderung["iterable_item_removed"]:
                this_aenderung = aenderung["iterable_item_removed"][key]
                if this_aenderung.type == "role":
                    text = "In Channel %s wurde die Rolle: %s(%s) entfernt." % (after.name, roles[this_aenderung.id], this_aenderung.id)
            roles = {}
        elif "iterable_item_added" in aenderung:
            roles = {}
            for rol in before.server.roles:
                roles[rol.id] = rol.name

            for key in aenderung["iterable_item_added"]:
                this_aenderung = aenderung["iterable_item_added"][key]
                if this_aenderung.type == "role":
                    text = "In Channel %s wurde die Rolle: %s(%s) hinzugefügt." % (after.name, roles[this_aenderung.id], this_aenderung.id)
            roles = {}
        if text != None:
            yield from client.send_message(client.get_channel(audit_log),text)

        # Called whenever a channel is updated. e.g. changed name, topic, permissions.
        return ""

    def use_as_on_member_join(self, member):
        # Called when a Member leaves or joins a Server.
        pass

    def use_as_on_member_remove(self, member):
        # Called when a Member leaves or joins a Server.
        pass

    def use_as_on_member_update(self, before, after):
        aenderung = deepdiff.DeepDiff(before, after)
        try:
            for key in aenderung["values_changed"]:
                try:
                    split_key = key.split(".")
                except AttributeError:
                    continue
                propertie = split_key[1]
                text = None
                if propertie[:5] == "roles" and split_key[2] == "hoist":
                    time.sleep(0.01)

                    before_roles = [(role.name,role.id) for role in before.roles]
                    after_roles = [(role.name,role.id) for role in after.roles]

                    rol_diff = deepdiff.DeepDiff(before_roles, after_roles)
                    rol_diff = rol_diff['values_changed']['root%s[0]'%propertie[5:]]

                    if len(before_roles) > len(after_roles):
                        role_text = "verloren"
                        role = rol_diff['old_value']

                    else:
                        role_text = "erhalten"
                        role = rol_diff['new_value']
                    text = "Member %s hat die Rolle %s %s" % (after.name,  role, role_text)

                if propertie == "nick":
                    text = "Member %s hat seinen Namen von %s in %s geändert" % (after.name, before.nick, after.nick)

                if text != None:
                    yield from client.send_message(client.get_channel(audit_log), text)

        except KeyError:
            pass

        # Called when a Member updates their profile. (status, game playing, avatar. nickname, roles)
        # print(str(before.nick) + str(after.nick))
        return ""

    def use_as_on_server_join(self, server):
        # Called when a Server is either created by the Client or when the Client joins a server.
        pass

    def use_as_on_server_remove(self, server):
        # Called when a Server is removed from the Client.
        pass

    def use_as_on_server_update(self, before, after):
        # Called when a Server updates, for example:
        pass

    def use_as_on_server_role_create(self, role):
        # Called when a Server creates or deletes a new Role.
        pass

    def use_as_on_server_role_delete(self, role):
        # Called when a Server creates or deletes a new Role.
        pass

    def use_as_on_server_emojis_update(self, before, after):
        # Called when a Server adds or removes Emoji.
        pass

    def use_as_on_server_available(self, server):
        # Called when a server becomes available or unavailable. The server must have existed in the Client.servers cache.
        pass

    def use_as_on_server_unavailable(self, server):
            member = server.members
            for m in member:
                if m.id in wichtige_user:
                    yield from client.send_message(m,content="Server: %s - %s ist gerade offline." % ( server.name, server.id))

        # Called when a server becomes available or unavailable. The server must have existed in the Client.servers cache.

    def use_as_on_member_ban(self, member):
        # Called when a Member gets banned from a Server.
        pass

    def use_as_on_member_unban(self, server, user):
        # Called when a User gets unbanned from a Server.
        pass

    def use_as_on_typing(self, channel, user, when):
        # Called when someone begins typing a message.
        pass

    def use_as_on_group_join(self, channel, user):
        # Called when someone joins or leaves a group, i.e. a PrivateChannel with a PrivateChannel.type of ChannelType.group.
        pass

    def use_as_on_group_remove(self, channel, user):
        # Called when someone joins or leaves a group, i.e. a PrivateChannel with a PrivateChannel.type of ChannelType.group.
        pass

if  "__main__" == __name__:
    client = discord.Client()
    Bot = Flo_Z_Bot()
    Bot.add_app(discord_bot.envents.Bot_events(client))
    Bot.add_app(discord_bot.member_activitaet.Log(client))
    Bot.add_app(discord_bot.TODO.Todo(client))
    Bot.add_app(discord_bot.polls.Polls(client))
    Bot.add_app(discord_bot.game_abo.Game_abos(client)) # Es muss die Rollen_id händisch vergeben werden.
    Bot.add_app(discord_bot.GUI.main_GUI(client, channels=wichtige_channel, bots=Bot.apps))
    # Bot.add_app(discord_bot.games.vour_in_a_row(client, channel_id="492019806048878593"))
    # Bot.add_app(discord_bot.RPG.my_rpg(client))

    @client.event
    @asyncio.coroutine
    def on_ready():
        yield from Bot.use_as_on_ready()

    @client.event
    @asyncio.coroutine
    def on_message(message):
        yield from Bot.use_as_on_message(message)

    @client.event
    @asyncio.coroutine
    def on_reaction_add(reaction, user):
        yield from Bot.use_as_on_reaction_add(reaction, user)

    @client.event
    @asyncio.coroutine
    def on_reaction_remove(reaction, user):
        yield from Bot.use_as_on_reaction_remove(reaction, user)

    @client.event
    @asyncio.coroutine
    def on_voice_state_update(before, after):
        yield from Bot.use_as_on_voice_state_update(before, after)

    @client.event
    @asyncio.coroutine
    def on_member_update(before, after):
        yield from Bot.use_as_on_member_update(before, after)

    @client.event
    @asyncio.coroutine
    def on_channel_update(before, after):
        yield from Bot.use_as_on_channel_update(before, after)

    @client.event
    @asyncio.coroutine
    def on_error(event, *args, **kwargs):
        yield from Bot.use_as_on_error(event, *args, **kwargs)

    while True:
        client.run(SECRETS.TOKEN)
        print("ERROR: der Server ist neu gestartet´´")