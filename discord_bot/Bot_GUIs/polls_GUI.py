import configparser
import discord

def reaction_handler(self, reaction, title):
    if (title == "Rollen"):
        yield from reaction_Rollen(self, reaction)
    if (title[:len("löschen einer Umfrage")] == "löschen einer Umfrage"):
        yield from reaction_Alle_Umfragen(self, reaction)
    if (title[:len("Chose a new Role")] == "Chose a new Role"):
        yield from  reaction_server_rollen(self, reaction)

def First(self,channel_id):
    bot = self.user[channel_id]['bots']
    em = Rollen(self, channel_id, bot)
    return em

def Rollen(self, channel_id, Bot):
    self.user[channel_id]['properties'] = []
    des = ""

    config = configparser.ConfigParser()
    config.sections()
    config.read(Bot.config_file)
    head = 0
    for serverid in config:
        if serverid != "DEFAULT":
            if serverid == self.user[channel_id]["server"].id:
                for index, use in enumerate(config[serverid]):
                    for rolobj in self.user[channel_id]["server"].roles:
                        rol = rolobj if rolobj.id == config[serverid][use] else None
                        if rol:
                            self.user[channel_id]['properties'].append([use,config[serverid][use]])
                            des += "%s| %s --> %s\n\n" % (self.my_GUI['emoji'][index + 1], str(use), rol.name)
    self.user[channel_id]["umfragen"] = []
    for sid in Bot.umfragen:
        self.user[channel_id]["umfragen"].append(Bot.umfragen[sid])
        head = (head +1) if Bot.umfragen[sid].msg.server.id == self.user[channel_id]["server"].id else head

    em = discord.Embed(
        title="%s: Rollen"%self.user[channel_id]['bots'].name,
        description="> %s aktive Umfragen\n\n%s"%(head,des),
        colour=discord.Colour.dark_magenta()
    )
    options = "%s| %s" % (self.my_GUI['emoji'][10], "anzeigen der Umfragen")
    em.add_field(name='Mögliche aktionen:', value=options, inline=False)

    em.set_footer(text="Server: %s --> Wer darf was" % (self.user[channel_id]["server"].name))
    return em

def reaction_Rollen(self, reaction):
    channel_id = reaction.message.channel.id

    for i in range(9):
        try:
            if reaction.emoji == self.my_GUI['emoji'][i + 1]:
                self.user[channel_id]["side"] = 0
                self.user[channel_id]['properties'] = self.user[channel_id]['properties'][i]
                yield from self.client.edit_message(message=reaction.message,
                                                    embed=server_rollen(self, channel_id))
        except:
            pass
    if reaction.emoji == self.my_GUI['emoji'][10]:
        self.user[channel_id]["side"] = 0
        yield from self.client.edit_message(message=reaction.message, embed=Alle_Umfragen(self, channel_id))

    if reaction.emoji == self.my_GUI['emoji'][13]:
        self.user[channel_id].pop("properties")
        self.user[channel_id].pop("umfragen")
        yield from self.client.edit_message(message=reaction.message, embed=self.Bots(channel_id))

def Alle_Umfragen(self, channel_id):
    side = self.user[channel_id]["side"]
    des = ""
    index = 0

    for item in self.user[channel_id]['umfragen'][side * 10:]:
        if index <= 9:
            des += "%s| %s --> %s \n\n" % (self.my_GUI['emoji'][index + 1], item.beschreibung, item.msg.channel.name)
        index = index + 1

    em = discord.Embed(
        title="%s: löschen einer Umfrage [%s / %s]"%(self.user[channel_id]['bots'].name, side + 1, int(len(self.user[channel_id]['umfragen']) / 10) + 1),
        description="%s"%(des),
        colour=discord.Colour.dark_magenta()
    )

    em.set_footer(text="Server: %s --> Bei auswahl einer umfrage wird diese inaktiv"%(self.user[channel_id]["server"].name))
    return em

def reaction_Alle_Umfragen(self, reaction):
    channel_id = reaction.message.channel.id
    side = self.user[channel_id]["side"]
    bot = self.user[channel_id]["bots"]

    for i in range(10):
        try:
            if reaction.emoji == self.my_GUI['emoji'][i + 1]:

                for n, u in enumerate(self.user[channel_id]['umfragen']):
                    if n == i + (side * 10):
                        bot.umfragen.pop(u.msg.id)
                        self.user[channel_id].pop("side")
                yield from self.client.edit_message(message=reaction.message,
                                                    embed=Rollen(self, channel_id, self.user[channel_id]["bots"]))
        except:
            pass

    if reaction.emoji == self.my_GUI['emoji'][11]:
        if side - 1 < 0:
            side = 0
        else:
            side = side - 1
        self.user[channel_id]["side"] = side
        yield from self.client.edit_message(message=reaction.message, embed=Alle_Umfragen(self,channel_id))

    if reaction.emoji == self.my_GUI['emoji'][12]:
        if side + 1 >= int(len(self.user[channel_id]['umfragen']) / 10) + 1:
            side = int(len(self.user[channel_id]['umfragen']) / 10)
        else:
            side = side + 1
        self.user[channel_id]["side"] = side
        yield from self.client.edit_message(message=reaction.message, embed=Alle_Umfragen(self,channel_id))

    if reaction.emoji == self.my_GUI['emoji'][13]:
        self.user[channel_id].pop("side")
        yield from self.client.edit_message(message=reaction.message, embed=Rollen(self, channel_id, self.user[channel_id]["bots"]))

def server_rollen(self, channel_id):
    side = self.user[channel_id]["side"]
    self.user[channel_id]["rollen"] = [rol for rol in self.user[channel_id]['server'].roles]

    des = ""
    index = 0

    for item in self.user[channel_id]['rollen'][side * 10:]:
        if index <= 9:
            des += "%s| %s --> %s \n\n" % (self.my_GUI['emoji'][index + 1], item.name, item.id)

        index = index + 1

    em = discord.Embed(
        title="%s: Chose a new Role [%s / %s]" % (self.user[channel_id]['bots'].name, side + 1, int(len(self.user[channel_id]['rollen']) / 10) + 1),
        description=des,
        colour=discord.Colour.dark_gold()
    )
    #self.user[channel_id]["side"] = side
    em.set_footer(text="Server: %s --> ändern der rolle" % (self.user[channel_id]["server"].name))
    return em


def reaction_server_rollen(self, reaction):
    channel_id = reaction.message.channel.id
    side = self.user[channel_id]["side"]
    bot = self.user[channel_id]["bots"]

    for i in range(10):
        try:

            if reaction.emoji == self.my_GUI['emoji'][i + 1]:

                change_role(self, channel_id,self.user[channel_id]['rollen'][i + (side * 10)])
                self.user[channel_id].pop("side")

                yield from self.client.edit_message(message=reaction.message,
                                                    embed=Rollen(self, channel_id, bot))
        except:
            pass

    if reaction.emoji == self.my_GUI['emoji'][11]:
        if (side - 1) < 0:
            side = 0
        else:
            side = side - 1
        self.user[channel_id]["side"] = side
        yield from self.client.edit_message(message=reaction.message, embed=server_rollen(self,channel_id))

    if reaction.emoji == self.my_GUI['emoji'][12]:
        if side + 1 >= int(len(self.user[channel_id]['rollen']) / 10) + 1:
            side = int(len(self.user[channel_id]['rollen']) / 10)
        else:
            side = side + 1
        self.user[channel_id]["side"] = side
        yield from self.client.edit_message(message=reaction.message, embed=server_rollen(self,channel_id))

    if reaction.emoji == self.my_GUI['emoji'][13]:
        self.user[channel_id].pop("side")
        yield from self.client.edit_message(message=reaction.message, embed=Rollen(self,channel_id, bot))

def change_role(self, channel_id, choice):
    bot = self.user[channel_id]["bots"]
    server_id = self.user[channel_id]["server"].id
    propertie = self.user[channel_id]["properties"]

    config = configparser.ConfigParser()
    config.sections()
    config.read(bot.config_file)

    config[server_id][propertie[0]] = str(choice.id)
    with open(bot.config_file, 'w') as configfile:
        config.write(configfile)

    bot.roles_update()