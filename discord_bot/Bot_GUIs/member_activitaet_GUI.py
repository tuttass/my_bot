import configparser
import discord
from datetime import datetime
import time
from beautifultable import BeautifulTable

def reaction_handler(self, reaction, title):
    if (title == "Options"):
        yield from reaction_Options(self, reaction)
    if (title == "Statistik"):
        yield from reaction_Statistik(self, reaction)

def First(self,channel_id):
    em = Options(self, channel_id)
    return em

def Options(self, channel_id):
    des = ""
    em = discord.Embed(
        title="%s: Options"%self.user[channel_id]['bots'].name,
        description=des,
        colour=discord.Colour.dark_magenta()
    )
    options = "%s| %s" % (self.my_GUI['emoji'][1], "Als Datei hochladen")
    options += "\n\n%s| %s" % (self.my_GUI['emoji'][2], "Statistik")
    em.add_field(name='Mögliche aktionen:', value=options, inline=False)

    footer = "Server: %s --> Wähle eine Option"%(self.user[channel_id]["server"].name)
    em.set_footer(text=footer)

    return em


def reaction_Options(self, reaction):
    channel_id = reaction.message.channel.id
    bot = self.user[channel_id]['bots']

    if reaction.emoji == self.my_GUI['emoji'][1]:
        if self.delete == None:
            yield from self.client.send_file(reaction.message.channel,content=POST_File(self, channel_id),fp=bot.channel_join_path[:-3] + "txt", filename="Aktivitaet.txt")

    if reaction.emoji == self.my_GUI['emoji'][2]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=Statistik(self, channel_id))

    if reaction.emoji == self.my_GUI['emoji'][13]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=self.Bots(channel_id))


def POST_File(self, channel_id):
    bot = self.user[channel_id]['bots']
    data = []
    server_id = self.user[channel_id]["server"].id
    config = configparser.ConfigParser()
    config.sections()
    config.read(bot.channel_join_path)

    for member in config[server_id]:
        info = config[server_id][member].split(";")
        data.append([info[0],int(info[1].split(".")[0])])

    data.sort(key=takesecond,reverse=True)

    day = 86400
    diese_woche = day * 7
    diesen_monat = day * 30
    dieses_jahr = day * 3650

    counter = [0,0,0,0]

    now = time.time()
    table7 = BeautifulTable()
    table7.column_headers = ["Name", "Datum"]
    table30 = BeautifulTable()
    table30.column_headers = ["Name", "Datum"]
    table365 = BeautifulTable()
    table365.column_headers = ["Name", "Datum"]
    tablelaenger = BeautifulTable()
    tablelaenger.column_headers = ["Name", "Datum"]

    for item in data:
        diff = now - item[1]
        t = datetime.fromtimestamp(item[1])
        zeit =  t.strftime('%d.%m.%Y')
        if diff < diese_woche:
            table7.append_row([item[0], zeit])
            counter[0] = counter[0] + 1
        elif diff < diesen_monat:
            table30.append_row([item[0], zeit])
            counter[1] = counter[1] + 1
        elif diff < dieses_jahr:
            table365.append_row([item[0], zeit])
            counter[2] = counter[2] + 1
        else:
            tablelaenger.append_row([item[0], zeit])
            counter[3] = counter[3] + 1

    text = "\n\n#### Letzen 7 Tage Online\n%s" \
           "\n\n#### Letzten 30 Tage Online\n%s" \
           "\n\n#### Letztn 365 Tage Online\n%s" \
           "\n\n#### Vor länger als einem Jahr Online\n%s"%(table7, table30, table365, tablelaenger)

    file = open(bot.channel_join_path[:-3]+"txt", 'w')
    file.write(text)
    file.close()

    des = "Datei_generiert (Zuletzt Aktiv)\n" \
          "Letzen 7 Tage: %s\n" \
          "Letzten 30 Tage: %s\n" \
          "Letztn 365 Tage: %s\n" \
          "Länge als ein Jahr: %s\n"%(counter[0],counter[1],counter[2],counter[3])

    return des

def Statistik(self, channel_id):
    bot = self.user[channel_id]['bots']

    config = configparser.ConfigParser()
    config.sections()
    config.read(bot.statistik_path)
    server_id = self.user[channel_id]["server"].id

    des = "Insgesammt: %s\n"%str(int(int(config[server_id]["insgesammt"])/2))

    for monat in config[server_id]:
        if monat != "insgesammt":
            try:
                info = monat.split(";")
                des += "%s.%s | %s\n"%(info[1], info[0], str(int(int(config[server_id][monat])/2)))
            except: pass

    em = discord.Embed(
        title="%s: Statistik" % bot.name,
        description=des,
        colour=discord.Colour.light_grey()
    )

    footer = "Server: %s --> Statistik wie oft jemand einen Channel gejoint ist."%(self.user[channel_id]["server"].name)
    em.set_footer(text=footer)

    return em

def reaction_Statistik(self, reaction):
    channel_id = reaction.message.channel.id

    if reaction.emoji == self.my_GUI['emoji'][13]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=Options(self, channel_id))

def takesecond(elem):
    return elem[1]