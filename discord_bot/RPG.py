import discord
from discord_bot.my_rpg.character import *
from discord_bot.my_rpg.objects import object
from discord_bot.my_rpg.sqlite_interface import DB
class my_rpg:
    def __init__(self, client):
        self.__client = client
        self.__emoji = ["✅","1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣", "🔟","⏪"]
        self.__user_objects = {}

    def use_on_message(self, message):
        if message.author == self.__client.user and str(message.channel.type) == "private":
            print("bot msg")
            return None
        if str(message.channel.type) != "private":
            return None


        if message.content.startswith('!start') and message.author != self.__client.user:
            if message.author.id in self.__user_objects:
                return None
            yield from self.__message_spiel_start(message)

        if message.content.startswith("!rm") and message.author != self.__client:
            if message.author.id not in self.__user_objects:
                return None
            yield from self.__rm_item_message(message)


    def use_on_reaction_add(self, reaction, user):
        return  self.__reaction_handler(reaction,user)

    def use_on_reaction_remove(self, reaction, user):
        return  self. __reaction_handler(reaction, user)

    def __reaction_handler(self, reaction,user):
        if str(reaction.message.channel.type) != "private":
            return ""
        if user == self.__client.user:
            return ""

        title = reaction.message.embeds[0]['title']
        if (title == "Home"):
            yield from self.__reaction_home(reaction, user)
        if (title == "Character"):
            yield from self.__reaction_character(reaction, user)
        if (title == "Equipment"):
            yield from self.__reaction_equipment(reaction, user)
        if (title == "Inventory"):
            yield from self.__reaction_equipment(reaction, user)
        if title.startswith("Attribute:"):
            yield from self.__reaction_attribute(reaction, user, title)

        return ""

    def __rm_item_message(self, message):
        player_obj = self.__user_objects[message.author.id]
        data = message.content[3:]
        try:
            list_ids = data.split(",")
        except:
            list_ids = [data, ]

        for index, id in enumerate(list_ids):
            print(str(index) + " - " + id)
            player_obj.delete_item_from_inventory(int(id)  -index)

        msg = yield from self.__client.send_message(message.channel, embed=discord.Embed(
            title="",
            description="",
            colour=discord.Colour.blue()))

        self.__user_objects[message.author.id].set_discord_message(msg)

        yield from self.__client.edit_message(message=msg,
                                              embed=self.__home(message.author.id))

        for i in self.__emoji:
            if i != self.__emoji[0]:
                yield from self.__client.add_reaction(msg, i)



    def __message_spiel_start(self, message):

        msg = yield from self.__client.send_message(message.channel, embed=discord.Embed(
            title="",
            description="",
            colour=discord.Colour.blue()))

        for i in self.__emoji:
            if i != self.__emoji[0]:
                yield from self.__client.add_reaction(msg, i)

        self.__user_objects[message.author.id] = player(name=message.author.name, life_points=100, mana=40)
        self.__user_objects[message.author.id].set_discord_user(message.author)
        self.__user_objects[message.author.id].set_discord_message(msg)

        em = self.__home(message.author.id)

        yield from self.__client.edit_message(message=msg,
                                        embed=em)

    def __reaction_home(self, reaction,user):
        if reaction.emoji == self.__emoji[1]:
            pass
        if reaction.emoji == self.__emoji[2]:
            yield from self.__client.edit_message(message=reaction.message, embed=self.__character(user.id))
        if reaction.emoji == self.__emoji[3]:
            yield from self.__client.edit_message(message=reaction.message, embed=self.__equipment(user.id))
        if reaction.emoji == self.__emoji[4]:
            yield from self.__client.edit_message(message=reaction.message, embed=self.__inventory(user.id))

    def __reaction_character(self, reaction,user):
        player_obj = self.__user_objects[user.id]
        for index, key in enumerate(player_obj.get_attributes.keys()):
            if reaction.emoji == self.__emoji[index+1]:
                yield from self.__client.edit_message(message=reaction.message, embed=self.__attribute(user.id, key))

        if reaction.emoji == self.__emoji[11]:
            yield from self.__client.edit_message(message=reaction.message, embed=self.__home(user.id))

    def __reaction_equipment(self, reaction, user):
        if reaction.emoji == self.__emoji[11]:
            yield from self.__client.edit_message(message=reaction.message, embed=self.__home(user.id))

    def __reaction_attribute(self, reaction, user, title):
        player_obj = self.__user_objects[user.id]
        atr = title[11:]

        if reaction.emoji == self.__emoji[1]:
            player_obj.update_attribute(atr, 1)
            yield from self.__client.edit_message(message=reaction.message, embed=self.__attribute(user.id, atr))

        if reaction.emoji == self.__emoji[2]:
            player_obj.update_attribute(atr, -1)
            yield from self.__client.edit_message(message=reaction.message, embed=self.__attribute(user.id, atr))

        if reaction.emoji == self.__emoji[11]:
            yield from self.__client.edit_message(message=reaction.message, embed=self.__character(user.id))

    def __reaction_inventory(self, reaction, user):
        if reaction.emoji == self.__emoji[11]:
            yield from self.__client.edit_message(message=reaction.message, embed=self.__home(user.id))

    def __home(self, user_id):

        player_obj = self.__user_objects[user_id]

        em = discord.Embed(
            title="Home",
            description="%s Kampf\n"
                        "%s Character\n"
                        "%s Equipment\n"
                        "%s Inventory"%(self.__emoji[1],self.__emoji[2],self.__emoji[3],self.__emoji[4]),
            colour=discord.Colour.blue()
        )
        em.set_author(name=player_obj.get_name)

        return em

    def __character(self, user_id):
        player_obj = self.__user_objects[user_id]
        description = ""
        index = 1
        for key,value in player_obj.get_attributes.items():
            description += self.__emoji[index]+ " " +key + ": " +str(value) +"\n"
            index = index + 1

        em = discord.Embed(
            title="Character",
            description=description,
            colour=discord.Colour.blue()
        )
        em.set_author(name=player_obj.get_name)
        return em

    def __equipment(self, user_id):
        player_obj = self.__user_objects[user_id]
        #player_obj.add_object(object(obj_type='armor'))
        description = ""
        index = 1
        for key,value in player_obj.get_equipment.items():
            if value == None:
                description += self.__emoji[index]+ " " +key + ": " +str(value) +"\n"
            else:
                description += self.__emoji[index] + " " + key + ": " + str(value.get_name) + "\n"
            index = index + 1

        em = discord.Embed(
            title="Equipment",
            description=description,
            colour=discord.Colour.blue()
        )
        em.set_author(name=player_obj.get_name)
        return em

    def __inventory(self, user_id):
        player_obj = self.__user_objects[user_id]

        player_obj.add_to_inventory(object(obj_type='armor'))
        description = "Space [%s/%s] \n\n id \n" % (len(player_obj.get_inventory), player_obj.get_inventory_space)
        for index, item in enumerate(player_obj.get_inventory):
            description += str(index)+ "\t\t" + item.get_name + " - " + item.get_type +'\n'

        em = discord.Embed(
            title="Inventory",
            description=description,
            colour=discord.Colour.blue()
        )
        em.set_author(name=player_obj.get_name)
        em.set_footer(text="To delete an object type !rm id1, id2")
        return em

    def __attribute(self, user_id, attribute_key):
        player_obj = self.__user_objects[user_id]
        description = "Current Points: " + str(player_obj.get_attributes[attribute_key]) + "\n\n" + self.__emoji[1] + " + \n"  + self.__emoji[2] + " - \n"

        em = discord.Embed(
            title="Attribute: " + attribute_key,
            description=description,
            colour=discord.Colour.blue()
        )
        em.set_author(name=player_obj.get_name)
        return em

if __name__=="__main__":
    object(obj_type="weapon", db_name="rpg.db")
    print(__name__)