import configparser
import discord

def reaction_handler(self, reaction, title):
    if title.startswith("Games"):
        yield from reaction_Games(self, reaction)
    if title.startswith("Chose a new Role"):
        yield from reaction_server_rollen(self, reaction)
    pass

def First(self,channel_id):
    bot = self.user[channel_id]['bots']
    em = Games(self, channel_id, bot)
    return em

def Games(self, channel_id, bot):
    self.user[channel_id]['channel'] = []
    des = ""

    counter = 0
    all_member = []
    for server in self.client.servers:
        if server.id == self.user[channel_id]['server'].id:
            all_member = server.members

    for msg_id in bot.messages.keys():
        c = 0
        r = bot.messages[msg_id]['rol']
        for member in all_member:
            user_roles_ids = []
            for rol in member.roles:
                user_roles_ids.append(rol.id)
            if r.id in user_roles_ids:
                c = c+1
        des += "%s - %s\n" %(bot.messages[msg_id]['rol'].name,c)
        counter = counter +1

    em = discord.Embed(
        title="%s: Games"%bot.name,
        description="> %s aktive Spiele\n\n"%counter + des,
        colour=discord.Colour.dark_grey()
    )


    options = "%s| %s" %(self.my_GUI['emoji'][10], "rol_to_del (%s)"%bot.del_rol[self.user[channel_id]['server'].id]['role_to_delete'])
    em.add_field(name='Mögliche aktionen:', value=options, inline=False)

    footer = "Wie viele haben die Rolle"
    em.set_footer(text=footer)

    return em

def reaction_Games(self, reaction):
    channel_id = reaction.message.channel.id
    if reaction.emoji == self.my_GUI['emoji'][10]:
        self.user[channel_id]["side"] = 0
        yield from self.client.edit_message(message=reaction.message,
                                            embed=server_rollen(self, channel_id))

    if reaction.emoji == self.my_GUI['emoji'][13]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=self.Bots(channel_id))

def server_rollen(self, channel_id):
    side = self.user[channel_id]["side"]
    self.user[channel_id]["rollen"] = [rol for rol in self.user[channel_id]['server'].roles]

    des = ""
    index = 0

    for item in self.user[channel_id]['rollen'][side * 10:]:
        if index <= 9:
            des += "%s| %s --> %s \n\n" % (self.my_GUI['emoji'][index + 1], item.name, item.id)

        index = index + 1

    em = discord.Embed(
        title="%s: Chose a new Role [%s / %s]" % (self.user[channel_id]['bots'].name, side + 1, int(len(self.user[channel_id]['rollen']) / 10) + 1),
        description=des,
        colour=discord.Colour.dark_gold()
    )
    em.set_footer(text="Server: %s --> ändern der rolle" % (self.user[channel_id]["server"].name))
    return em

def reaction_server_rollen(self, reaction):
    channel_id = reaction.message.channel.id
    side = self.user[channel_id]["side"]
    bot = self.user[channel_id]["bots"]

    for i in range(10):
        try:

            if reaction.emoji == self.my_GUI['emoji'][i + 1]:
                change_role(self, channel_id, self.user[channel_id]["rollen"][i + (side * 10)])
                self.user[channel_id].pop("side")

                yield from self.client.edit_message(message=reaction.message,
                                                    embed=Games(self, channel_id, bot))
        except:
            pass

    if reaction.emoji == self.my_GUI['emoji'][11]:
        if side - 1 < 0:
            side = 0
        else:
            side = side - 1
        self.user[channel_id]["side"] = side
        yield from self.client.edit_message(message=reaction.message, embed=server_rollen(self,channel_id))

    if reaction.emoji == self.my_GUI['emoji'][12]:
        if side + 1 >= int(len(self.user[channel_id]['rollen']) / 10) + 1:
            side = int(len(self.user[channel_id]['rollen']) / 10)
        else:
            side = side + 1
        self.user[channel_id]["side"] = side
        yield from self.client.edit_message(message=reaction.message, embed=server_rollen(self,channel_id))

    if reaction.emoji == self.my_GUI['emoji'][13]:
        self.user[channel_id].pop("side")
        yield from self.client.edit_message(message=reaction.message, embed=Games(self, channel_id,bot))

def change_role(self, channel_id, choice):
    bot = self.user[channel_id]["bots"]
    server_id = self.user[channel_id]["server"].id

    config = configparser.ConfigParser()
    config.sections()
    config.read(bot.config_file)

    config[server_id]['role_to_delete'] = str(choice.id)
    with open(bot.config_file, 'w') as configfile:
        config.write(configfile)

    bot.update_roles()