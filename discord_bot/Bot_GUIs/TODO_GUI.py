import discord
import configparser

def reaction_handler(self, reaction, title):
    if (title == "Channel"):
        yield from reaction_Channel(self, reaction)
    if (title[:12] == "Chose a new "):
        yield from reaction_choice(self, reaction)

def First(self,channel_id):
    em = Channel(self, channel_id)
    return em

def Channel(self, channel_id):
    bot = self.user[channel_id]['bots']
    self.user[channel_id]['channel'] = []
    count = 0
    for l in bot.todo.keys():
        if bot.todo[l]["message"] != "None":
            if self.client.get_channel(l).server.id == self.user[channel_id]['server'].id:
                count = count + 1
    des = "> %s aktive Listen\n\n"%count

    config = configparser.ConfigParser()
    config.sections()
    config.read(bot.channel_file)
    index = 0
    for id in config:
        if id != "DEFAULT":
            ch = self.client.get_channel(id)
            if ch.server.id == self.user[channel_id]["server"].id:
                self.user[channel_id]['channel'].append(ch)
                des += "%s| %s --> %s\n\n" % (self.my_GUI['emoji'][index + 1], str(ch.name), str(ch.id))
                index = index + 1
    em = discord.Embed(
        title="%s: Channel"%self.user[channel_id]['bots'].name,
        description=des,
        colour=discord.Colour.dark_magenta()
    )

    options = "%s| %s" % (self.my_GUI['emoji'][10], "Bot in einem neuen Channel hinzufügen")
    em.add_field(name='Mögliche aktionen:', value=options, inline=False)

    footer = "Server: %s --> Wähle einen Channel in denen Befehle ausgeführt werden können"%(self.user[channel_id]["server"].name)
    em.set_footer(text=footer)

    return em


def reaction_Channel(self, reaction):
    channel_id = reaction.message.channel.id
    for i in range(9):
        try:
            if reaction.emoji == self.my_GUI['emoji'][i + 1]:
                self.user[channel_id]['channel'] = self.user[channel_id]['channel'][i]
                yield from self.client.edit_message(message=reaction.message,
                                                    embed=choice(self, channel_id, 0))
        except:
            pass
    if reaction.emoji == self.my_GUI['emoji'][10]:
        self.user[channel_id]['extras'] = ["channel",]
        yield from self.client.edit_message(message=reaction.message,
                                            embed=choice(self, channel_id, 0))

    if reaction.emoji == self.my_GUI['emoji'][13]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=self.Bots(channel_id))
def choice(self, channel_id, side):
    des = ""
    index = 0
    Name = ""
    if type(self.user[channel_id]['channel']) == list:
        side_len = 10
    else:
        side_len = 9

    if side == 0:
        self.user[channel_id]['new'] = []

        Name = "channel"
        footer = "Wähle einen channel mit der rolle 'Bot_'"
        for s in self.client.servers:
            if s.id == self.user[channel_id]['server'].id:
                for c in s.channels:
                    for rol in c.changed_roles:
                        if rol.name == "Bot_":
                            if index in range (0,side_len):
                                des += "%s| %s --> %s \n\n" % (self.my_GUI['emoji'][index + 1], c.name, c.id)
                            self.user[channel_id]['new'].append(c)
                            index = index + 1
    else:
        Name = "channel"
        footer = "Wähle einen channel mit der rolle 'BOT_'"
        for item in self.user[channel_id]['new'][side * side_len:]:
            if index in range (0,side_len):
                des += "%s| %s --> %s \n\n" % (self.my_GUI['emoji'][index + 1], item.name, item.id)
            index = index + 1

    em = discord.Embed(
        title="%s: Chose a new %s [%s / %s]" % (self.user[channel_id]['bots'].name,Name, side + 1, int(len(self.user[channel_id]['new']) / side_len) + 1),
        description=des,
        colour=discord.Colour.dark_gold()
    )
    if side_len == 9:
        em.add_field(name='Mögliche aktionen:', value="%s| Bot aus channel: %s löschen \n\n" % (self.my_GUI['emoji'][10], self.user[channel_id]['bots'].name), inline=False)

    self.user[channel_id]["side"]=side


    em.set_footer(text="Server: %s --> "%(self.user[channel_id]["server"].name)+footer)
    return em


def reaction_choice(self, reaction):

    channel_id = reaction.message.channel.id
    side = self.user[channel_id]["side"]
    content = False
    for i in range(0,10):
        try:
            if reaction.emoji == self.my_GUI['emoji'][i + 1]:
                if type(self.user[channel_id]['channel']) == list:
                    content = add_channel(self.user[channel_id]['bots'], self.user[channel_id]['new'][i + (side * 10)].id)
                else:
                    if reaction.emoji == self.my_GUI['emoji'][10]:
                        content = remove_channel(self.user[channel_id]['bots'], self.user[channel_id]['channel'].id)

                    else:
                        content = change_channel(self.user[channel_id]['bots'], self.user[channel_id]['channel'].id,
                                     self.user[channel_id]['new'][i + (side * 9)].id)

                if content != False:
                    yield from self.client.edit_message(message=reaction.message, embed=Channel(self, channel_id))

        except:
            pass

    if reaction.emoji == self.my_GUI['emoji'][11]:
        if side - 1 < 0:
            side = 0
        else:
            side = side - 1
        yield from self.client.edit_message(message=reaction.message, embed=choice(self,channel_id, side))

    if reaction.emoji == self.my_GUI['emoji'][12]:
        if side + 1 >= int(len(self.user[channel_id]['new']) / 10) + 1:
            side = int(len(self.user[channel_id]['new']) / 10)
        else:
            side = side + 1
        yield from self.client.edit_message(message=reaction.message, embed=choice(self, channel_id, side))

    if reaction.emoji == self.my_GUI['emoji'][13]:
        yield from self.client.edit_message(message=reaction.message,
                                            embed=Channel(self, channel_id))

def add_channel(Bot, after ):
    if after in Bot.todo.keys():
        return False
    config = configparser.ConfigParser()
    config.sections()
    config.read(Bot.channel_file)

    config[after] = {}
    config[after]["message"] = "None"

    with open(Bot.channel_file, 'w') as configfile:
        config.write(configfile)
    Bot.channel_update()

def change_channel(Bot, before, after):

    if before in Bot.todo.keys():
        if Bot.todo[before]["message"] != "None":
            return False

    if after in Bot.todo.keys():
        if Bot.todo[after]["message"] != "None":
            return False

    config = configparser.ConfigParser()
    config.sections()
    config.read(Bot.channel_file)

    config.pop(before)
    config[after] = {}
    config[after]["message"] = "None"

    with open(Bot.channel_file, 'w') as configfile:
        config.write(configfile)
    Bot.channel_update()

def remove_channel(Bot, before):
    if Bot.todo[before]["message"] != "None":
        return False
    config = configparser.ConfigParser()
    config.sections()
    config.read(Bot.channel_file)

    config.pop(before)

    with open(Bot.channel_file, 'w') as configfile:
        config.write(configfile)

    Bot.channel_update()