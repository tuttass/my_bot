from discord_bot.my_rpg.sqlite_interface import DB
from random import *
from discord_bot.my_rpg.sql_vocabulary import *

class weapon:
    def __init__(self, db, id):
        self.__set_weapon_attributes(db, id)

    def __set_weapon_attributes(self, db, id):
        data = db.execute("SELECT * FROM weapon where id = %s;" %id)[0]
        self.__damage = data[1]
        self.__type = data[2]
        self.__effect = data[3]
        self.__element = data[4]

    @property
    def get_weapon_damage(self): return self.__damage

    @property
    def get_weapon_type(self): return WEAPON['type'][self.__type]

    @property
    def get_weapon_effect(self): return WEAPON['effect'][self.__effect]

    @property
    def get_weapon_element(self): return WEAPON['element'][self.__element]

class head:
    def __init__(self):
        pass
class chest:
    def __init__(self):
        pass
class poor:
    def __init__(self):
        print("test")
class legs:
    def __init__(self):
        pass
class shoes:
    def __init__(self):
        pass

class armor(head):
    def __init__(self,db,id):
        self.__set_armor_attributes(db,id)
        self.__heir_bodypart()


    def __set_armor_attributes(self, db, id):
        data = db.execute("SELECT * FROM armor where id = %s;" % id)[0]
        self.__armor = data[1]
        self.__magic_armor = data[2]
        self.__type = data[3]
        self.__bodypart = data[4]

    def __heir_bodypart(self):
        if self.__bodypart == 1:
            head.__init__(self)
        if self.__bodypart == 2:
            chest.__init__(self)
        if self.__bodypart == 3:
            poor.__init__(self)
        if self.__bodypart == 4:
            legs.__init__(self)
        if self.__bodypart == 5:
            shoes.__init__(self)

    @property
    def get_armor(self): return self.__armor

    @property
    def get_magic_armor(self): return self.__magic_armor

    @property
    def get_armor_type(self): return ARMOR['type'][self.__type]

    @property
    def get_armor_bodypart(self): return ARMOR['bodypart'][self.__bodypart]


class object(weapon, armor):
    def __init__(self, obj_type=None, id=None, create_object=True):
        self.__db =  DB(database="rpg.db")
        self.__type_str = None
        self.__id = None

        #import pdb; pdb.set_trace()

        if create_object == True:

            if obj_type and not id:
                id  = self.create_object_by_type(obj_type)

            self.__set_attributes_by_id(id)

            if self.__type_str == "armor":
                self.__obj_type = armor
            if self.__type_str == "weapon":
                self.__obj_type = weapon


            self.__obj_type.__init__(self, self.__db ,id)

    def create_object_by_type(self, type=None):
        my_list = self.__db.execute("SELECT id, drop_rate FROM object where type = %s;"%(OBJECT['type'][type]))
        my_random_choice_list = []
        for item in my_list:
            for m in range(item[1]):
                my_random_choice_list.append(item[0])

        new_object_id = int(choice(my_random_choice_list))
        return new_object_id


    def __set_attributes_by_id(self, id):
        data = self.__db.execute("SELECT * FROM object where id = %s;" % id)[0]
        self.__id = data[0]
        self.__name = data[1]
        self.__type_str = OBJECT['type'][data[2]]
        self.__weight = data[3]
        self.__drop_rate = data[4]
        self.__quality = data[5]
        self.__status = data[6]
        self.__discription = data[7]

    @property
    def get_id(self): return self.__id

    @property
    def get_name(self): return self.__name

    @property
    def get_type(self): return self.__type_str

    @property
    def get_weight(self): return self.__weight

    @property
    def get_drop_rate(self): return self.__drop_rate

    @property
    def get_quality(self): return OBJECT['quality'][self.__quality]

    @property
    def get_status(self): return OBJECT['status'][self.__status]

    @property
    def get_discription(self): return self.__discription


if __name__ == "__main__":
    o = object(obj_type=weapon)
    print(str(o.get_id)+o.get_name+str(o.get_weight)+str(o.get_drop_rate)+o.get_quality+o.get_status+o.get_discription)
    #print(str(o.get_weapon_damage)+o.get_weapon_effect+o.get_weapon_element+o.get_weapon_type)

    o = object(id = 5)