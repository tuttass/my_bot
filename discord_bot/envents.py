import discord
import discord_bot.my_events as event
from time import gmtime, strftime
from datetime import datetime
import configparser
import discord_bot.Bot_GUIs.Event_Bot_GUI as Event_Bot
from revisiondict import RevisionDict

class Bot_events:
    def __init__(self, client):
        self.client = client
        self.name = "Event_Bot"
        self.bot_module = Event_Bot
        self.events = {
            'current_creater': None,
            'current_msg': None,
            'beschreibung': "",
            'emoji': ["➕", "➖", "❔", "☠", "❗"],
            'stage': 0,
            'time': 0,
            'name': "",
            'max': 0,
            'channel_ids': {},
            'list': {},
        }
        self.channel_update()

    def channel_update(self):
        self.events['channel_ids'] = {}
        config = configparser.ConfigParser()
        config.sections()
        config.read('discord_bot/config/GUI_%s.cfg'%self.name)
        for my_event in config:
            if my_event != "DEFAULT":

                self.events['channel_ids'][my_event] = {}
                self.events['channel_ids'][my_event]['channel_erstellt'] = config[my_event]["channel_erstellt"]
                self.events['channel_ids'][my_event]['channel_geloescht'] = config[my_event]["channel_geloescht"]
                self.events['channel_ids'][my_event]['rolle'] = config[my_event]["rolle"]

    def use_on_ready(self):

        yield from self.readIniFile(0)
        yield from self.writeIniFile(0)
        pass

    def use_on_message(self, message):
        if message.channel.id not in self.events['channel_ids'].keys():
            return ""

        if message.content.startswith("!event") and self.events['current_creater'] == None:
            if len(message.content) > 6:
                yield from self.quick_event(message)
                return ""


        if message.content.startswith("!save"):

            user_roles_ids = []
            for rol in message.author.roles:
                user_roles_ids.append(rol.id)
            if self.events['channel_ids'][message.channel]['rolle'] in user_roles_ids:
                yield from self.writeIniFile(message.author)
            yield from self.client.delete_message(message)

        if message.content.startswith("!load"):
            user_roles_ids = []
            for rol in message.author.roles:
                user_roles_ids.append(rol.id)
            if self.events['channel_ids'][message.channel]['rolle'] in user_roles_ids:
                yield from self.readIniFile(message.author)
            yield from self.client.delete_message(message)

        if message.author != self.events['current_creater'] and message.author != self.client.user and self.events['current_creater'] != None :
            yield from self.client.delete_message(message)

        return ""

    def use_on_reaction_add(self, reaction, user):
        if reaction.message.channel.id not in self.events['channel_ids'].keys() or user == self.client.user:
            return ""

        if reaction.message.id not in self.events['list'].keys():
            return ""

        myevent = self.events['list'][reaction.message.id]
        if user.id not in myevent.deny and user.id not in myevent.maybe and user.id not in myevent.accept:
            if reaction.emoji == self.events['emoji'][0]:

                if int(len(myevent.accept)) >= int(myevent.event_max):
                    return ""

                myevent.accept[user.id] = user
                yield from self.event_print(myevent)
                yield from self.writeIniFile(0)

            if reaction.emoji == self.events['emoji'][1]:
                myevent.deny[user.id] = user
                yield from self.event_print(myevent)
                yield from self.writeIniFile(0)

            if reaction.emoji == self.events['emoji'][2]:
                myevent.maybe[user.id] = user
                yield from self.event_print(myevent)
                yield from self.writeIniFile(0)

        if reaction.emoji == self.events['emoji'][3]:
            user_roles_ids = []
            for rol in user.roles:
                user_roles_ids.append(rol.id)

            if myevent.creator.id == user.id or self.events['channel_ids'][reaction.message.channel.id]['rolle'] in user_roles_ids:
                em = discord.Embed(title =reaction.message.embeds[0]['title'], description = reaction.message.embeds[0]['description'])
                data = reaction.message.embeds[0]['fields']
                for d in data:
                    em.add_field(name=d['name'] , value=d['value'], inline=True)
                em.set_footer(text=reaction.message.embeds[0]['footer']['text'])
                if user.nick:
                    yield from self.client.send_message(self.client.get_channel(self.events['channel_ids'][reaction.message.channel.id]['channel_geloescht']),
                                                        "___\n" + "Message id: " + self.events['list'][reaction.message.id].started_message_id + "\nDiscordname: " + user.name + "\nServername:" + user.nick, embed=em)
                else:
                    yield from self.client.send_message(self.client.get_channel(self.events['channel_ids'][reaction.message.channel.id]['channel_geloescht']),
                                                        "___\n" + "Message id: " + self.events['list'][reaction.message.id].started_message_id + "\nDiscordname: " + user.name + "\nServername:" + "None",
                                                        embed=em)

                self.events['list'].pop(reaction.message.id)
                yield from self.client.delete_message(reaction.message)
                yield from self.writeIniFile(0)

        if reaction.emoji == self.events['emoji'][4] and myevent.creator.id == user.id:
                self.events['list'][reaction.message.id].importend = True
                yield from self.event_print(myevent)
        return ""

    def use_on_reaction_remove(self, reaction, user):
        if reaction.message.channel.id not in self.events['channel_ids'].keys() or user == self.client.user:
            return ""

        if reaction.message.id not in self.events['list'].keys():
            return ""

        myevent = self.events['list'][reaction.message.id]
        if reaction.emoji == self.events['emoji'][0] and user.id in myevent.accept:
            myevent.accept.pop(user.id)
            yield from self.event_print(myevent)
            yield from self.writeIniFile(0)

        if user.id in myevent.deny and reaction.emoji == self.events['emoji'][1]:
            myevent.deny.pop(user.id)
            yield from self.event_print(myevent)
            yield from self.writeIniFile(0)

        if user.id in myevent.maybe and reaction.emoji == self.events['emoji'][2]:
            myevent.maybe.pop(user.id)
            yield from self.event_print(myevent)
            yield from self.writeIniFile(0)

        if reaction.emoji == self.events['emoji'][4] and myevent.creator == user:
            myevent.importend = False
            yield from self.event_print(myevent)
            yield from self.writeIniFile(0)
        return ""

    def get_my_color(self, current, max):
        percent = current/max
        # blau grün rot
        blau = 0
        gruen = (255 * percent) if (percent < 0.5) else 200
        rot = 255 - (255 * percent) if (percent > 0.5) else 200

        color = discord.Colour(blau + int(gruen) * 256 + int(rot) * (256 ** 2))

        return color

    def event_print(self, myevent):

        em = discord.Embed(
        title = myevent.event_name,
        description = myevent.event_time,
        colour = self.get_my_color(len(myevent.accept), myevent.event_max)
        )

        a = ""
        for i in myevent.accept:
            try:
                a+=myevent.accept[i].nick+"\n"
            except: a+=myevent.accept[i].name+"\n"
        d = ""

        for i in myevent.deny:
            try:
                d+=myevent.deny[i].nick+"\n"
            except: d+=myevent.deny[i].name+"\n"
        m = ""
        for i in myevent.maybe:
            try:
                m+=myevent.maybe[i].nick+"\n"
            except: m+=myevent.maybe[i].name+"\n"

        if a == "":
            a = "-"
        if d == "":
            d = "-"
        if m == "":
            m = "-"



        em.add_field(name='Akzeptiert [%s/%s]'%(len(myevent.accept), myevent.event_max), value=a, inline=True)
        em.add_field(name='Abgelehnt [%s]'%(len(myevent.deny)), value=d, inline=True)
        em.add_field(name='Vorbehalt [%s]'%(len(myevent.maybe)), value=m, inline=True)

        em.set_footer(text="Ps: %s"%myevent.beschreibung)

        if myevent.importend == True:
            return self.client.edit_message(message=myevent.msg, new_content="@here",
                                             embed=em)

        return self.client.edit_message(message=myevent.msg, new_content=" ",
                                             embed=em)

    def quick_event(self, message):
        values = message.content.split(";")
        msg = yield from self.client.send_message(message.channel, embed=discord.Embed(title= "Error",description = " ! Maxteilnehmer muss eine Zahl sein !!",colour = discord.Colour.blue() ))
        try:
            self.events['list'][msg.id] = event.event(message.author,
                                                      msg,
                                                      values[2],
                                                      values[1],
                                                      int(values[3]),
                                                      values[4],
                                                      )
            self.events['list'][msg.id].started_message_id = msg.id
            yield from self.event_print(self.events['list'][msg.id])
            yield from self.client.add_reaction(msg, self.events['emoji'][0])
            yield from self.client.add_reaction(msg, self.events['emoji'][1])
            yield from self.client.add_reaction(msg, self.events['emoji'][2])
            yield from self.client.add_reaction(msg, self.events['emoji'][4])
            yield from self.client.add_reaction(msg, self.events['emoji'][3])

            if message.author.nick:
                yield from self.client.send_message(self.client.get_channel(self.events['channel_ids'][message.channel.id]['channel_erstellt']), "___\n" + "Message id: " + msg.id + "\nDiscordname: " + message.author.name + "\nServername: " + message.author.nick + "\nRollen: "+ str([r.name for r in message.author.roles]).replace("@", "_") +"\nEventbefehl: " + message.content + "\nUhrzeit: " + datetime.now().strftime("%d.%m.%Y - %H:%M Uhr")+ "\n")
            else:
                yield from self.client.send_message(self.client.get_channel(self.events['channel_ids'][message.channel.id]['channel_erstellt']), "___\n" + "Message id: " + msg.id + "\nDiscordname: " + message.author.name + "\nServername: " + "None" + "\nRollen: " + str([r.name for r in message.author.roles]).replace("@","_") + "\nEventbefehl: " + message.content + "\nUhrzeit: " + datetime.now().strftime("%d.%m.%Y - %H:%M Uhr") + "\n")

            yield from self.client.delete_message(message)
        except: pass

    def writeIniFile(self, user):
        config = configparser.ConfigParser()
        count = 0
        ids = []
        for main_key, t in self.events['list'].items():
            accept = ""
            for key, value in t.accept.items():
                accept += str(value.id) + ","
            deny = ""
            for key, value in t.deny.items():
                deny += str(value.id) + ","
            maybe = ""
            for key, value in t.maybe.items():
                maybe += str(value.id) + ","


            config[main_key] = {}
            config[main_key]["creator_id"] = "%s"%t.creator.id
            config[main_key]["message_id"] = "%s"%t.msg.id
            config[main_key]["started_message_id"] = "%s"%t.started_message_id
            config[main_key]["importend"] = "%s"%t.importend
            config[main_key]["event_time"] = "%s"%t.event_time
            config[main_key]["event_name"] = "%s"%t.event_name
            config[main_key]["event_max"] = "%s"%t.event_max
            config[main_key]["beschreibung"] = "%s"%t.beschreibung
            config[main_key]["accept"] = "%s"%accept
            config[main_key]["deny"] = "%s"%deny
            config[main_key]["maybe"] = "%s"%maybe
            count = count +1
            ids.append(t.started_message_id)
            pass
        with open('discord_bot/my_events/Events.cfg', 'w') as configfile:
            config.write(configfile)

        if user == 0:
            return None
        saved = ""
        for id in ids:
            saved+= " --> "+str(id) +"\n"
        yield from self.client.send_message(user, saved+" Es wurden %s Events gespeichert."%(count))

    def readIniFile(self, user):
        self.events['list']={}
        config = configparser.ConfigParser()
        config.sections()
        config.read('discord_bot/my_events/Events.cfg')
        count = 0
        ids=[]
        errors={}
        channels = []
        for c in self.events['channel_ids'].keys():
            channels.append(self.client.get_channel(c))

        for my_event in config:
            if my_event != "DEFAULT":
                try:
                    ids.append(config[my_event]["started_message_id"])
                    message = None
                    for c in channels:
                        try:
                            message = yield from self.client.get_message(c, config[my_event]['message_id'])
                            for m in c.server.members:
                                if m.id == config[my_event]['creator_id']:
                                    this_member = m
                        except: pass

                    msg = yield from self.client.send_message(message.channel, embed=
                                                                                    discord.Embed(
                                                                                    title = "Error",
                                                                                    description = " ! Maxteilnehmer muss eine Zahl sein !!",
                                                                                    colour = discord.Colour.blue()
                                                                                    ))

                    self.events['list'][msg.id] = event.event(creator=this_member,
                                                                msg=msg,
                                                                event_time=config[my_event]['event_time'],
                                                                event=config[my_event]['event_name'],
                                                                max=int(config[my_event]['event_max']),
                                                                beschreibung=config[my_event]['beschreibung'],
                                                                )

                    a = RevisionDict()
                    for i in config[my_event]['accept'].split(','):
                        for member in msg.server.members:
                            if i == member.id:
                                a[member.id] = member

                    d = RevisionDict()
                    for i in config[my_event]['deny'].split(','):
                        for member in msg.server.members:
                            if i == member.id:
                                d[member.id] = member
                    m = RevisionDict()
                    for i in config[my_event]['maybe'].split(','):
                        for member in msg.server.members:
                            if i == member.id:
                                m[member.id] = member

                    self.events['list'][msg.id].accept = a
                    self.events['list'][msg.id].deny = d
                    self.events['list'][msg.id].maybe = m
                    self.events['list'][msg.id].importend = config[my_event]['importend']
                    self.events['list'][msg.id].started_message_id = config[my_event]["started_message_id"]

                    yield from self.event_print(self.events['list'][msg.id])
                    yield from self.client.add_reaction(msg, self.events['emoji'][0])
                    yield from self.client.add_reaction(msg, self.events['emoji'][1])
                    yield from self.client.add_reaction(msg, self.events['emoji'][2])
                    yield from self.client.add_reaction(msg, self.events['emoji'][4])
                    yield from self.client.add_reaction(msg, self.events['emoji'][3])

                    yield from self.client.delete_message(message)
                    count = count +1

                except:
                    if ids != []:
                        errors[ids[-1]]= " - Import hat nicht Funktioniert !!!"
                    print(" Import hat nicht Funktioniert")
        config.clear()

        if user == 0:
            return None
        saved=""
        for id in ids:
            try:
                saved += " --> " + str(id) + errors[id] + "\n"
            except:
                saved += " --> " + str(id) + "\n"

        yield from self.client.send_message(user, saved+"Import Fertig ! \nEs wurden %s Events von %s geladen. "%(count,len(ids)))
