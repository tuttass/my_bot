import sqlite3
from discord_bot.my_rpg.sql_vocabulary import *

class DB:

    def __init__(self, database='../rpg.db'):

        self.database = database
        self.display = False

        self.connect()

    def connect(self):

        self.connection = sqlite3.connect(self.database)
        self.cursor = self.connection.cursor()
        self.connected = True

    def close(self, data=None):
        """Close the SQLite3 database."""
        self.connection.commit()
        self.connection.close()
        self.connected = False

    def execute(self, statement):
        if not self.connected:
            #open a previously closed connection
            self.connect()
            #mark the connection to be closed once complete
        if type(statement) != str:
            return False

        try:
            self.cursor.execute(statement)
            data = self.cursor.fetchall()

        except sqlite3.Error as error:
            print ('An error occurred:', error.args[0])
            print ('For the statement:', statement)
            return False

        if not data:
            return False
        self.close()
        return data

    def add_object(self, name, type, weight, drop_rate, quality, status, discription, **kwargs ):
        self.execute("INSERT INTO "
                   "object (name, type, weight, drop_rate, quality, status, discription ) "
                   "VALUES("
                   "'%s', '%s', %s, %s, %s, %s, '%s')"%(name, OBJECT['type'][type], weight, drop_rate, quality, status, discription) )
        id = "(select distinct(id) from object order by id desc )"
        if type == "weapon":
            self.execute("INSERT INTO "
                       "weapon (id, damage, weapon_type, effect, element) "
                       "VALUES("
                       "%s, %s, %s, %s, %s)"%(id,kwargs['damage'],kwargs['weapon_type'],kwargs['effect'],kwargs['element']))
        if type == "armor":
            self.execute("INSERT INTO "
                         "armor (id, armor, magic_armor, type, bodypart) "
                         "VALUES("
                         "%s, %s, %s, %s, %s)" % (id, kwargs['armor'],kwargs['magic_armor'],kwargs['armor_type'],kwargs['bodypart']))

if __name__ == '__main__':
    db = DB()
    db.execute("CREATE TABLE IF NOT EXISTS object( "
               "id INTEGER PRIMARY KEY AUTOINCREMENT,"
               "name varchar(255) ,"
               "type INTERGER ,"
               "weight INTEGER ,"
               "drop_rate INTEGER,"
               "quality INTEGER,"
               "status INTEGER,"
               "discription varchar(255)"
               ");")
    db.execute("CREATE TABLE IF NOT EXISTS weapon ("
               "id INTEGER PRIMARY KEY,"
               "damage INTEGER,"
               "weapon_type INTEGER,"
               "effect INTEGER,"
               "element INTEGER,"
               "FOREIGN KEY(id) REFERENCES object(id)"
               ");")
    db.execute("CREATE TABLE IF NOT EXISTS armor ("
               "id INTEGER PRIMARY KEY,"
               "armor INTEGER,"
               "magic_armor INTEGER,"
               "type INTEGER,"
               "bodypart INTEGER,"
               "FOREIGN KEY(id) REFERENCES object(id)"
               ");")
    db.add_object(name="iron sword",type="weapon",weight=5,drop_rate=10, quality=OBJECT['quality']['ordinary'],status=OBJECT['status']['normal'], discription='some discription',
                  damage=5, weapon_type=WEAPON['type']['meele'],effect=1 ,element=WEAPON['element']['None'])
    db.add_object(name="Bow", type="weapon", weight=3, drop_rate=10, quality=OBJECT['quality']['ordinary'],status=OBJECT['status']['normal'], discription='some discription',
                  damage=5, weapon_type=WEAPON['type']['range'], effect=1, element=WEAPON['element']['None'])
    db.add_object(name="magic wand", type="weapon", weight=2, drop_rate=10, quality=OBJECT['quality']['ordinary'],status=OBJECT['status']['normal'], discription='some discription',
                  damage=5, weapon_type=WEAPON['type']['magic'], effect=1, element=WEAPON['element']['None'])


    db.add_object(name="helmet", type="armor", weight=3, drop_rate=4, quality=OBJECT['quality']['ordinary'], status=OBJECT['status']['normal'], discription='some discription',
                  armor=5, magic_armor=0, armor_type=ARMOR['type']['medium'], bodypart=ARMOR['bodypart']['head'])

    db.add_object(name="breastplate", type="armor", weight=10, drop_rate=4, quality=OBJECT['quality']['ordinary'], status=OBJECT['status']['normal'], discription='some discription',
                  armor=20, magic_armor=5, armor_type=ARMOR['type']['medium'], bodypart=ARMOR['bodypart']['chest'])

    db.add_object(name="arm splints", type="armor", weight=4, drop_rate=4, quality=OBJECT['quality']['ordinary'], status=OBJECT['status']['normal'], discription='some discription',
                  armor=10, magic_armor=2, armor_type=ARMOR['type']['medium'], bodypart=ARMOR['bodypart']['poor'])

    db.add_object(name="leg splints", type="armor", weight=4, drop_rate=4, quality=OBJECT['quality']['ordinary'], status=OBJECT['status']['normal'], discription='some discription',
                  armor=10, magic_armor=2, armor_type=ARMOR['type']['medium'], bodypart=ARMOR['bodypart']['legs'])

    db.add_object(name="shoes", type="armor", weight=2, drop_rate=4, quality=OBJECT['quality']['ordinary'], status=OBJECT['status']['normal'], discription='some discription',
                  armor=3, magic_armor=2, armor_type=ARMOR['type']['medium'], bodypart=ARMOR['bodypart']['shoes'])

    #db.execute("INSERT INTO weapon(id, damage) VALUES(1, 10),(2,7),(3,5)")
    print(db.execute("SELECT * FROM object "))
    print(db.execute("SELECT * FROM weapon "))
    print(db.execute("SELECT * FROM armor "))
    #db.close()