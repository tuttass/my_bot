import discord
import discord_bot.Bot_GUIs.Event_Bot_GUI as Event_Bot

class main_GUI:
    def __init__(self, client, channels, bots):
        self.client = client
        self.name = "GUI"
        self.user ={}

        self.my_GUI = {
            "channels": channels,
            "emoji": ["✅","1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣", "🔟" ,"⬅", "➡", "⏪"],
            "bots": bots,
        }
        self.modules={}
        self.delete= None

        self.define_user()
        self.define_modules()

    def define_user(self, channel_id=None):
        if channel_id==None:
            self.user={}
            for c in self.my_GUI["channels"]:
                self.user[c] = {
                    "server": None,
                    "bots": None,
                }
        else:
            self.user[channel_id] = {
                "server": None,
                "bots": None,
            }
    def define_modules(self):
        self.modules={}
        for b in self.my_GUI["bots"]:
            self.modules[b.name] = b.bot_module


    def use_on_message(self, message):
        if message.channel.id not in self.my_GUI['channels']:
            return ""
        if message.author == self.client.user:
            if message.content.startswith('Datei_generiert'):
                self.delete = message
            return ""

        if message.content.startswith('!Menü'):
            yield from self.__message_menu(message)

        return ""

    def use_on_reaction_add(self, reaction, user):
        if reaction.message.channel.id not in self.my_GUI['channels'] or user == self.client.user:
            return ""
        return self.__reaction_handler(reaction, user)

    def use_on_reaction_remove(self, reaction, user):
        if reaction.message.channel.id not in self.my_GUI['channels'] or user == self.client.user:
            return ""
        return self.__reaction_handler(reaction, user)

    def __reaction_handler(self, reaction, user):

        title = reaction.message.embeds[0]['title']
        if (title == "Server"):
            yield from self.reaction_Server(reaction)
        elif (title == "Bots"):
            yield from self.reaction_Bot(reaction)

        elif title.startswith(self.user[reaction.message.channel.id]["bots"].name):
            bot_name = self.user[reaction.message.channel.id]["bots"].name
            yield from self.modules[bot_name].reaction_handler(self, reaction, title[len(bot_name)+2:])
        if self.delete != None:
            if type(self.delete) == list:
                yield from self.client.delete_message(self.delete[0])
                self.delete = None
            else:
                d = self.delete
                self.delete = []
                self.delete.append(d)
        #yield from self.client.remove_reaction(reaction.message, reaction.emoji, user)
        return ""

    def __message_menu(self, message):

        msg = yield from self.client.send_message(message.channel, embed=discord.Embed(
            title="",
            description="",
            colour=discord.Colour.blue()))

        for i in self.my_GUI['emoji']:
            if i != self.my_GUI['emoji'][0]:
                yield from self.client.add_reaction(msg, i)

        em = self.Server(message.channel.id)
        yield from self.client.edit_message(message=msg,embed=em)

    def Server(self, channel_id):
        self.define_user(channel_id=channel_id)
        des = ""
        self.user[channel_id]['server'] = []
        for index, server in enumerate(self.client.servers):
            self.user[channel_id]['server'].append(server)
            des += "%s| %s\n\n" % (self.my_GUI['emoji'][index + 1], str(server.name))

        em = discord.Embed(
            title="Server",
            description=des,
            colour=discord.Colour.blue()
        )
        return em

    def reaction_Server(self, reaction):
        channel_id = reaction.message.channel.id
        for i in range(10):
            try:
                if reaction.emoji == self.my_GUI['emoji'][i+1]:
                    self.user[channel_id]['server']=self.user[channel_id]['server'][i]
                    yield from self.client.edit_message(message=reaction.message, embed=self.Bots(channel_id))
            except: pass

    def Bots(self, channel_id):
        server = self.user[channel_id]["server"]
        self.define_user(channel_id=channel_id)
        self.user[channel_id]["server"] = server

        des=""
        self.user[channel_id]['bots'] = []
        for index, Bot in enumerate(self.my_GUI['bots']):
            if index != len(self.my_GUI['bots']) -1:
                self.user[channel_id]['bots'].append(Bot)
                des += "%s| %s\n\n" % (self.my_GUI['emoji'][index+1], str(Bot.name))

        em = discord.Embed(
            title="Bots",
            description=des,
            colour=discord.Colour.blue()
        )

        em.set_footer(text="Wähle einen Bot")
        return em

    def reaction_Bot(self, reaction):
        channel_id = reaction.message.channel.id
        for i in range(10):
            try:
                if reaction.emoji == self.my_GUI['emoji'][i+1]:
                    self.user[channel_id]['bots']=self.user[channel_id]['bots'][i]

                    yield from self.client.edit_message(message=reaction.message, embed=self.modules[self.user[channel_id]['bots'].name].First(self, channel_id))
            except: pass
        if reaction.emoji == self.my_GUI['emoji'][13]:
            yield from self.client.edit_message(message=reaction.message, embed=self.Server(channel_id))