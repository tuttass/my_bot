import time
from discord_bot.my_games.viergewinnt import *
import random

class vour_in_a_row:
    def __init__(self, client, channel_id):
        self.client = client
        self.player = {
            1: None,
            2: None,
            'channel_id': channel_id,
            'last_invite': 0,
            'ingame': False,
            'brett': None,
            'input': ["!0", "!1", "!2", "!3", "!4", "!5", "!6"],
            'game': None,
            'clear': [],
        }
        self.reactions = ["1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣"]

    def use_on_message(self, message):

        if message.channel.id == self.player['channel_id']:
            found = False
            if message.content == "!viergewinnt" and self.player['ingame'] == False:
                found = True
                if (time.time() - self.player['last_invite']) < 120 or self.player['last_invite'] == 0:
                    if self.player[1] == message.author:
                        yield from self.client.delete_message(message)
                        return None

                    if self.player[1] == None:
                        self.player[1] = message.author
                        self.player['last_invite'] = time.time()
                        msg = yield from self.client.send_message(self.client.get_channel(self.player['channel_id']),
                                                             self.player[1].name + " want to play join him")
                        yield from self.client.add_reaction(msg, "✅")
                        self.player['clear'].append(msg)

                    else:
                        yield from self.second_player(message.author)

                    yield from self.client.delete_message(message)
                else:
                    yield from self.client.delete_message(message)

                    self.player[1] = None
                    self.player['last_invite'] = 0

                    msg = yield from self.client.send_message(self.client.get_channel(self.player['channel_id']),
                                                         "Versuch es nochmal")
                    self.player['clear'].append(msg)

            if self.player['ingame'] == True and message.content in self.player['input']:
                found = True
                if message.author == self.player['game'].get_current_player:
                    yield from self.reaction_viergewinnt(int(message.content[1:]))
                else:
                    yield from self.client.delete_message(message)

            if message.author != self.client.user and message.author != self.player[1] and message.author != self.player[2]:
                found = True
                yield from self.client.delete_message(message)

            if found == False and message.author != self.client.user:
                self.player['clear'].append(message)

    def use_on_reaction_add(self, reaction, user):
        if reaction.message.channel.id == self.player['channel_id']:
            if self.player[2] == None and user != self.client.user and reaction.emoji == "✅":
                if self.player[1] != user:
                    if self.player[1] != None:
                        yield from self.second_player(user)
                if self.player[1] == user:
                    msg = yield from self.client.send_message(self.client.get_channel(self.player['channel_id']), "!viergewinnt")
                    self.player['clear'].append(msg)
                    self.player[2] = self.client.user

            if user == self.client.user:
                return None

            if self.player['game'].get_current_player == user and self.player[2] != None:
                if self.client.user and reaction.emoji == "✅" or self.player['brett'].id != reaction.message.id:
                    return None
                yield from self.reaction_viergewinnt(self.reactions.index(reaction.emoji))

                    
    def use_on_reaction_remove(self, reaction, user):

        if reaction.message.channel.id == self.player['channel_id']:
            if self.player['game'].get_current_player == user:
                if self.player['brett'].id == reaction.message.id:
                    yield from self.reaction_viergewinnt(self.reactions.index(reaction.emoji))


    def reaction_viergewinnt(self, cul, mymessage=None):
        for x in self.player['clear']:
            try:
                yield from self.client.delete_message(x)
            except:
                pass
        self.player['clear'] = []
        try:
            self.player['game'].add_stone(cul)

            yield from self.client.edit_message(message=self.player['brett'],
                                           new_content="```%s```" % self.player['game'].printField())
            win = self.player['game'].checkWin()
            if win != None:
                yield from self.client.send_message(self.client.get_channel(self.player['channel_id']),
                                               "Der gewinner ist :**" + self.player[
                                                   'game'].get_current_player.name + "**")
                self.clear()
            elif len(self.player['game'].get_history) == 42:
                yield from self.client.send_message(self.client.get_channel(self.player['channel_id']), "Unendschieden")
                self.clear()
            else:
                msg = yield from self.client.send_message(self.client.get_channel(self.player['channel_id']), "**" + self.player[
                    'game'].get_current_player.name + "** ist an der Reihe")
                self.player['clear'].append(msg)
                if self.player[2] == self.client.user and self.player['game'].get_current_player == self.client.user:
                    msg = yield from self.bot_action()
                    self.player['clear'].append(msg)

            if mymessage != None:
                yield from self.client.delete_message(mymessage)
        except:
            if self.player['game'].get_current_player.id == self.client.user.id:
                msg = yield from self.bot_action()
                self.player['clear'].append(msg)
            else:
                msg = yield from self.client.send_message(self.client.get_channel(self.player['channel_id']),
                                                     "Spalte voll versuch es nochmal")
                self.player['clear'].append(msg)

    def add_riactions(self, emoji):
        return self.client.add_reaction(self.player['brett'], emoji)

    def second_player(self, user):
        self.player[2] = user
        self.player['game'] = Game(self.player[1], self.player[2])
        self.player['brett'] = yield from self.client.send_message(self.client.get_channel(self.player['channel_id']),
                                                              "```%s```" % self.player['game'].printField())
        for i in self.reactions:
            yield from self.add_riactions(i)

        msg = yield from self.client.send_message(self.client.get_channel(self.player['channel_id']),
                                             "**" + self.player[
                                                 'game'].get_current_player.name + "** ist an der Reihe")
        self.player['clear'].append(msg)
        self.player['ingame'] = True

    def clear(self):
        self.player[1] = None
        self.player[2] = None
        self.player['last_invite'] = 0
        self.player['ingame'] = False
        self.player['game'] = None

    def bot_action(self):
        check = self.player['game'].check_next_round()
        if check == None:
            check = random.choice(list(self.player['game'].get_input.keys()))
            #print("choice: " + str(check))
        return self.client.send_message(self.client.get_channel(self.player['channel_id']), "!%s" % (str(check)))

