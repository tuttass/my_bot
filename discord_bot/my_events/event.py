import time
from revisiondict import RevisionDict

class event:
    def __init__(self, creator, msg, event_time, event, max, beschreibung):
        self.creator = creator
        self.createt_time = time.time()
        self.msg = msg
        self.started_message_id = None
        self.importend = False


        self.event_time = event_time
        self.event_name = event
        self.event_max = max
        self.beschreibung = beschreibung

        self.accept = RevisionDict()
        self.deny = RevisionDict()
        self.maybe = RevisionDict()

    def print(self):
        text = "%s" \
               "%s \n\n" \
               "%s" % (self.header, self.beschreibung, self.Tabelle)
        return text
    @property
    def list_accepted(self):
        text = []
        for user_id in self.accept:
            if self.accept[user_id] != None:
                text.append(self.accept[user_id].name)
        return text

    @property
    def list_denyed(self):
        text = []
        for user_id in self.deny:
            if self.deny[user_id] != None:
                text.append(self.deny[user_id].name)
        return text

    @property
    def list_maybe(self):
        text = []
        for user_id in self.maybe:
            if self.maybe[user_id] != None:
                text.append(self.maybe[user_id].name)
        return text

    @property
    def header(self):
        text = ""

        name = self.event_name
        text += name
        for i in range(0, 40 - len(name)):
            text+=" "
        text += self.event_time+ "\n"
        return text
    @property
    def Tabelle(self):
        value = 20
        accept = self.list_accepted
        deny = self.list_denyed
        maybe = self.list_maybe
        count = 0
        text = ""
        a_text = "accepted [%s/%s]"%(len(accept), self.event_max)
        d_text = "denied [%s]"%len(deny)
        m_text = "maybe [%s]"%len(maybe)

        len_a_text = value - len(a_text)
        len_d_text = value - len(d_text)
        len_m_text = value - len(m_text)

        text += a_text
        for i in range(0,len_a_text):
            text += " "
        text += d_text
        for i in range(0,len_d_text):
            text += " "
        text += m_text
        for i in range(0,len_m_text):
            text += " "
        text += "\n"
        #if int(self.event_max) > 9:
            #text += "accepted [%s/%s]     denied [%s]          maybe [%s]          \n"%(len(accept), self.event_max, len(deny), len(maybe))
        #else:
            #text += "accepted [%s/%s]      denied [%s]          maybe [%s]          \n"%(len(accept), self.event_max, len(deny), len(maybe))


        if len(accept) < len(deny):
            count = len(deny)
        elif len(accept) < len(maybe):
            count = len(maybe)
        else:
            count = len(accept)

        for i in range(0,count):
            try:
                a = accept[i]
                if len(a) > 18:
                    a = a[:18]
                    a += "~"
            except:
                a = ""
            try:
                d = deny[i]
                if len(d) > 18:
                    d = d[:18]
                    d += "~"
            except:
                d = ""
            try:
                m = maybe[i]
                if len(m) > 18:
                    m = m[:18]
                    m += "~"
            except:
                m = ""

            la = 20 - len(a)
            ld = 20 - len(d)
            lm = 20 - len(m)

            text += a
            for i in range (0, la):
                text += " "
            text += d
            for i in range(0, ld):
                text += " "
            text += m
            for i in range(0, lm):
                text += " "
            text += "\n"

        return text
